/**
 * Created by joe_t on 1/3/2017.
 */

// http://www.jvandemo.com/how-to-configure-your-angularjs-application-using-environment-variables/

'use strict';
window.__env = window.__env || {};

// API url
window.__env.api_url = 'http://localhost';

// port
window.__env.port = 3000;

// Base url
window.__env.base_url = '/';

// Whether or not to enable debug mode
// Setting this to false will disable console output
window.__env.enable_debug = true;
