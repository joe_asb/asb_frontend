/**
 * Created by joe_t on 12/29/2016.
 */
(function () {
'use strict';

var env = {};

// Import variables if present (from env.js)
if(window){
    Object.assign(env, window.__env);
}

var main_app = angular
        .module('asb_main', ['ui.router', 'ngStorage', 'angular-jwt', 'ngMaterial', 'ngFileUpload'])
        .config(_config)
        .constant('__env', env)
        .run(_run)
;

function _config($stateProvider, $urlRouterProvider, $httpProvider) {

    $urlRouterProvider.otherwise('/');
    // $httpProvider.defaults.useXDomain = true;
    // delete $httpProvider.defaults.headers.common['X-Requested-With'];

    $stateProvider
        .state('index', {
            url: '/',
            views: {
                'v_global': {
                    templateUrl: 'html/view_global.html',
                    controller: 'index_controller',
                    controllerAs: 'vm'
                },
                'v_main@index': {
                    templateUrl: 'html/view_global_index.html',
                    controller: 'index_controller',
                    controllerAs: 'vm'
                },
                'v_nav@index': {
                    templateUrl: 'html/view_global_nav.html',
                    controller: 'nav_controller',
                    controllerAs: 'vm'
                },
                'v_footer@index': {
                    templateUrl: 'html/view_global_footer.html',
                    controller: 'footer_controller',
                    controllerAs: 'vm'
                }
            }
        })
        .state('login', {
            url: '/login',
            views: {
                'v_global': {
                    templateUrl: 'html/view_login.html',
                    controller: 'login_controller',
                    controllerAs: 'vm'
                }
            }
        })
        .state('profile', {
            url: '/profile',
            views: {
                'v_global': {
                    templateUrl: 'html/view_global.html',
                    controller: 'index_controller',
                    controllerAs: 'vm'
                },
                'v_main@profile': {
                    templateUrl: 'html/view_global_profile.html',
                    controller: 'profile_controller',
                    controllerAs: 'vm'
                },
                'v_nav@profile': {
                    templateUrl: 'html/view_global_nav.html',
                    controller: 'nav_controller',
                    controllerAs: 'vm'
                },
                'v_footer@profile': {
                    templateUrl: 'html/view_global_footer.html',
                    controller: 'footer_controller',
                    controllerAs: 'vm'
                }
            }
        })
        .state('clients', {
            url: '/clients',
            views: {
                'v_global': {
                    templateUrl: 'html/view_global.html',
                    controller: 'index_controller',
                    controllerAs: 'vm'
                },
                'v_main@clients': {
                    templateUrl: 'html/view_global_clients.html',
                    controller: 'client_controller',
                    controllerAs: 'vm'
                },
                'v_nav@clients': {
                    templateUrl: 'html/view_global_nav.html',
                    controller: 'nav_controller',
                    controllerAs: 'vm'
                },
                'v_footer@clients': {
                    templateUrl: 'html/view_global_footer.html',
                    controller: 'footer_controller',
                    controllerAs: 'vm'
                }
            }
        })
        .state('seats', {
            url: '/seats',
            views: {
                'v_global': {
                    templateUrl: 'html/view_global.html',
                    controller: 'index_controller',
                    controllerAs: 'vm'
                },
                'v_main@seats': {
                    templateUrl: 'html/view_global_seats.html',
                    controller: 'seat_controller',
                    controllerAs: 'vm'
                },
                'v_nav@seats': {
                    templateUrl: 'html/view_global_nav.html',
                    controller: 'nav_controller',
                    controllerAs: 'vm'
                },
                'v_footer@seats': {
                    templateUrl: 'html/view_global_footer.html',
                    controller: 'footer_controller',
                    controllerAs: 'vm'
                }
            }
        })
        .state('projects_list', {
            url: '/projects_list',
            views: {
                'v_global': {
                    templateUrl: 'html/view_global.html',
                    controller: 'index_controller',
                    controllerAs: 'vm'
                },
                'v_main@projects_list': {
                    templateUrl: 'html/view_global_projects.html',
                    controller: 'seat_controller',
                    controllerAs: 'vm'
                },
                // 'v_project_communities@projects': {
                //     templateUrl: 'html/view_project_communities.html',
                //     controller: 'project_controller',
                //     controllerAs: 'vm'
                // },
                // 'v_project_accounts@projects': {
                //     templateUrl: 'html/view_project_accounts.html',
                //     controller: 'project_controller',
                //     controllerAs: 'vm'
                // },
                // 'v_project_projects@projects': {
                //     templateUrl: 'html/view_project_list.html',
                //     controller: 'project_controller',
                //     controllerAs: 'vm'
                // },
                // 'v_project_stories@projects': {
                //     templateUrl: 'html/view_project_stories.html',
                //     controller: 'project_controller',
                //     controllerAs: 'vm'
                // },
                'v_projects_content@projects_list': {
                    templateUrl: 'html/view_project_list.html',
                    controller: 'project_controller',
                    controllerAs: 'vm'
                },
                'v_nav@projects_list': {
                    templateUrl: 'html/view_global_nav.html',
                    controller: 'nav_controller',
                    controllerAs: 'vm'
                },
                'v_footer@projects_list': {
                    templateUrl: 'html/view_global_footer.html',
                    controller: 'footer_controller',
                    controllerAs: 'vm'
                }
            }
        })
        .state('communities_list', {
            url: '/communities_list',
            views: {
                'v_global': {
                    templateUrl: 'html/view_global.html',
                    controller: 'index_controller',
                    controllerAs: 'vm'
                },
                'v_main@communities_list': {
                    templateUrl: 'html/view_global_communities.html',
                    // controller: 'community_controller',
                    controllerAs: 'vm'
                },
                // 'v_project_communities@projects': {
                //     templateUrl: 'html/view_project_communities.html',
                //     controller: 'project_controller',
                //     controllerAs: 'vm'
                // },
                // 'v_project_accounts@projects': {
                //     templateUrl: 'html/view_project_accounts.html',
                //     controller: 'project_controller',
                //     controllerAs: 'vm'
                // },
                // 'v_project_projects@projects': {
                //     templateUrl: 'html/view_project_list.html',
                //     controller: 'project_controller',
                //     controllerAs: 'vm'
                // },
                // 'v_project_stories@projects': {
                //     templateUrl: 'html/view_project_stories.html',
                //     controller: 'project_controller',
                //     controllerAs: 'vm'
                // },
                'v_communities_content@communities_list': {
                    templateUrl: 'html/view_community_list.html',
                    controller: 'community_controller',
                    controllerAs: 'vm'
                },
                'v_nav@communities_list': {
                    templateUrl: 'html/view_global_nav.html',
                    controller: 'nav_controller',
                    controllerAs: 'vm'
                },
                'v_footer@communities_list': {
                    templateUrl: 'html/view_global_footer.html',
                    controller: 'footer_controller',
                    controllerAs: 'vm'
                }
            }
        })
        .state('projects_details_accounts', {
            url: '/projects_details_accounts',
            views: {
                'v_global': {
                    templateUrl: 'html/view_global.html',
                    controller: 'index_controller',
                    controllerAs: 'vm'
                },
                'v_main@projects_list': {
                    templateUrl: 'html/view_global_projects.html',
                    controller: 'seat_controller',
                    controllerAs: 'vm'
                },
                'v_projects_content@projects_list': {
                    templateUrl: 'html/view_project_list.html',
                    controller: 'project_controller',
                    controllerAs: 'vm'
                },
                'v_projects_content@projects_details_accounts': {
                    templateUrl: 'html/view_project_details.html',
                    controller: 'project_controller',
                    controllerAs: 'vm'
                },
                'v_nav@projects_list': {
                    templateUrl: 'html/view_global_nav.html',
                    controller: 'nav_controller',
                    controllerAs: 'vm'
                },
                'v_footer@projects_list': {
                    templateUrl: 'html/view_global_footer.html',
                    controller: 'footer_controller',
                    controllerAs: 'vm'
                }
            }
        })
        .state('projects_details_communities', {
            url: '/projects_details_communities',
            views: {
                'v_global': {
                    templateUrl: 'html/view_global.html',
                    controller: 'index_controller',
                    controllerAs: 'vm'
                },
                'v_main@projects_list': {
                    templateUrl: 'html/view_global_projects.html',
                    controller: 'seat_controller',
                    controllerAs: 'vm'
                },
                'v_projects_content@projects_list': {
                    templateUrl: 'html/view_project_list.html',
                    controller: 'project_controller',
                    controllerAs: 'vm'
                },
                'v_projects_content@projects_details_communities': {
                    templateUrl: 'html/view_project_communities.html',
                    controller: 'project_controller',
                    controllerAs: 'vm'
                },
                'v_nav@projects_list': {
                    templateUrl: 'html/view_global_nav.html',
                    controller: 'nav_controller',
                    controllerAs: 'vm'
                },
                'v_footer@projects_list': {
                    templateUrl: 'html/view_global_footer.html',
                    controller: 'footer_controller',
                    controllerAs: 'vm'
                }
            }
        })
        .state('projects_stories', {
            url: '/projects_stories',
            views: {
                'v_global': {
                    templateUrl: 'html/view_global.html',
                    controller: 'index_controller',
                    controllerAs: 'vm'
                },
                'v_main@projects_stories': {
                    templateUrl: 'html/view_global_projects.html',
                    controller: 'seat_controller',
                    controllerAs: 'vm'
                },
                // 'v_project_communities@projects': {
                //     templateUrl: 'html/view_project_communities.html',
                //     controller: 'project_controller',
                //     controllerAs: 'vm'
                // },
                // 'v_project_accounts@projects': {
                //     templateUrl: 'html/view_project_accounts.html',
                //     controller: 'project_controller',
                //     controllerAs: 'vm'
                // },
                // 'v_project_projects@projects': {
                //     templateUrl: 'html/view_project_list.html',
                //     controller: 'project_controller',
                //     controllerAs: 'vm'
                // },
                // 'v_project_stories@projects': {
                //     templateUrl: 'html/view_project_stories.html',
                //     controller: 'project_controller',
                //     controllerAs: 'vm'
                // },
                'v_projects_content@projects_stories': {
                    templateUrl: 'html/view_project_stories.html',
                    controller: 'project_controller',
                    controllerAs: 'vm'
                },
                'v_nav@projects_stories': {
                    templateUrl: 'html/view_global_nav.html',
                    controller: 'nav_controller',
                    controllerAs: 'vm'
                },
                'v_footer@projects_stories': {
                    templateUrl: 'html/view_global_footer.html',
                    controller: 'footer_controller',
                    controllerAs: 'vm'
                }
            }
        })
        .state('accounts', {
            url: '/accounts_list',
            views: {
                'v_global': {
                    templateUrl: 'html/view_global.html',
                    controller: 'index_controller',
                    controllerAs: 'vm'
                },
                'v_main@accounts': {
                    templateUrl: 'html/view_global_accounts.html',
                    controller: 'account_controller',
                    controllerAs: 'vm'
                },
                'v_nav@accounts': {
                    templateUrl: 'html/view_global_nav.html',
                    controller: 'nav_controller',
                    controllerAs: 'vm'
                },
                'v_footer@accounts': {
                    templateUrl: 'html/view_global_footer.html',
                    controller: 'footer_controller',
                    controllerAs: 'vm'
                }
            }
        })
        .state('platforms', {
            url: '/platforms_list',
            views: {
                'v_global': {
                    templateUrl: 'html/view_global.html',
                    controller: 'index_controller',
                    controllerAs: 'vm'
                },
                'v_main@platforms': {
                    templateUrl: 'html/view_global_platforms.html',
                    controller: 'platform_controller',
                    controllerAs: 'vm'
                },
                'v_nav@platforms': {
                    templateUrl: 'html/view_global_nav.html',
                    controller: 'nav_controller',
                    controllerAs: 'vm'
                },
                'v_footer@platforms': {
                    templateUrl: 'html/view_global_footer.html',
                    controller: 'footer_controller',
                    controllerAs: 'vm'
                }
            }
        })
    ;

    // $http is a service; $httpProvider is a provider to customize the global behavior of $http
    // via the $httpProvider.defaults.headers configuration object
    // https://docs.angularjs.org/api/ng/service/$http
    // https://docs.angularjs.org/api/ng/provider/$httpProvider
    // https://www.toptal.com/web/cookie-free-authentication-with-json-web-tokens-an-example-in-laravel-and-angularjs
    $httpProvider.interceptors.push(['$q', '$location', '$localStorage', '$sessionStorage',
        function ($q, $location, $localStorage, $sessionStorage) {
            return {
                'request': function (config) {
                    config.headers = config.headers || {};
                    if ($localStorage.currentUser && $sessionStorage.currentUser) {
                        config.headers.Authorization = $localStorage.currentUser.token;
                        // console.log('[52] currentUser: ' + $sessionStorage.currentUser.username);
                    }
                    return config;
                },
                'response': function (response) {
                    if ((response.headers('Authorization') !== null) && (typeof($localStorage.currentUser) !== 'undefined')) {
                        $localStorage.currentUser.token = response.headers('Authorization');
                    }
                    return response;
                },
                'responseError': function (response) {
                    if (response.status === 401 || response.status === 403) {
                        $location.path('/login');
                    }
                    return $q.reject(response);
                }
            };
        }
    ]);

}

function _run($rootScope, $http, $location, $localStorage, $sessionStorage) {
    // keep user logged in after page refresh
    if ($localStorage.currentUser && $sessionStorage.currentUser) {
        $http.defaults.headers.common.Authorization = $localStorage.currentUser.token;
    }

    var current_year = new Date().getFullYear();
    $rootScope.$on('$locationChangeStart', function(event, next, current, $moment) {
        $rootScope._app_title = "Afferent :: Welcome";
        $rootScope._footer_blurb = "Copyright " + current_year + " Afferent SB";
        $rootScope._small_logo = '/img/asb_logo.png';
        $rootScope._large_logo = '/img/asb_logo.png';
        $rootScope._bg_color_header = '#FFFFFF';
        $rootScope._bg_color_footer = '#FFFFFF';
        var public_pages = ['/login'];
        var restricted_page = public_pages.indexOf($location.path()) === -1;
        if (restricted_page && !$localStorage.currentUser && !$sessionStorage.currentUser) {
            $location.path('/login');
        }
    })
}

})();
