'use strict';

angular
    .module('asb_main')
    .controller('nav_controller', _controller);

function _controller($scope, __env, ClientRolesService, $localStorage) {
    $scope.message;
    var vm = this;
    vm.role_id = ($localStorage.currentUser.role_id !== undefined) ? $localStorage.currentUser.role_id : -1;

    _init();

    function _init() {
        get_client_roles();
        console.log(vm.role_id);
    }

    function get_client_roles() {
        ClientRolesService
            ._get_client_roles()
            .then(
                function(client_roles) {
                    vm.client_roles = client_roles;
                    vm.client_roles_with_keys = {};
                    for (var _cnt = 0; _cnt < vm.client_roles.length; _cnt++) {
                        var _role = vm.client_roles[_cnt];
                        vm.client_roles_with_keys[_role['id']] = _role;
                    }
                }
                , function(err) {
                    console.log(err);
                }
            )
    }
}
