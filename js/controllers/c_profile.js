'use strict';

angular
    .module('asb_main')
    .controller('profile_controller', _controller);

function _controller($scope, $rootScope, $localStorage, UsersService, ClientsService, ClientRolesService) {
    var vm = this;
    vm.update_profile = update_profile();

    _init();

    function _init() {
        $rootScope._state_title = 'User Profile';
        vm._this_user_email = $localStorage.currentUser.email;
        get_clients();
        get_client_roles();
        get_this_user();
    }

    function get_this_user() {
        UsersService
            ._get_user($localStorage.currentUser.user_id)
            .then(
                function(user) {
                    vm.user = user;
                }
                , function(err) {
                    console.log(err);
                }
            );
    }

    function get_clients() {
        ClientsService
            ._get_clients()
            .then(
                function(clients) {
                    vm.clients = clients;
                }
                , function(err) {
                    console.log(err);
                }
            )
    }

    function get_client_roles() {
        ClientRolesService
            ._get_client_roles()
            .then(
                function(client_roles) {
                    vm.client_roles = client_roles;
                }
                , function(err) {
                    console.log(err);
                }
            )
    }

    function update_profile() {
        console.log('This is still a stub.');
    }
}
