'use strict';

angular
    .module('asb_main')
    .controller('account_controller', _controller);

function _controller($scope, $rootScope, $timeout, $localStorage, ClientRolesService, AccountService, ClientProjectAccountService, PlatformService, $mdDialog) {
    $scope.message;
    var vm = this;
    vm.this_account = {};

    vm.this_account_reset = function() {
        vm.this_account.screen_name = '';
        vm.this_account.ref_platform_id = -1;
        vm.this_output = {
            offset: 1,
            limit: 10,
            offsets: [1, 2, 3, 4, 5],
            limits: [10, 20, 50]
        }
    };

    vm.status_reset = function() {
        vm.status = '';
        vm.status_class = '';
    };

    _init();

    function _init() {

        $rootScope._state_title = 'accounts';
        is_global_admin();
        vm.this_account_reset();
        get_accounts();
        get_platforms();
    }

    // From https://stackoverflow.com/a/14438954/6554056
    function onlyUnique(value, index, self) {
        return self.indexOf(value) === index;
    }

    function get_platforms() {
        PlatformService
            ._get_platforms()
            .then(
                function(platforms) {
                    vm.platforms = platforms;

                }
                , function(err) {
                    console.log(err);
                }
            )
    }

    function is_global_admin() {
        console.log('[54] in is_global_admin');
        ClientRolesService
            ._get_client_role($localStorage.currentUser.role_id)
            .then(
                function(client_role) {
                    vm.client_role = client_role.role;
                    $localStorage.currentUser.is_global_admin = (vm.client_role == 'All Client Admin');
                    $scope.is_global_admin = (vm.client_role == 'All Client Admin');
                    console.log($scope.is_global_admin);
                }
                , function(err) {
                    console.log(err);
                }
            );
    }

    function get_accounts() {
        var _account_query_form = {
            tab_client_seat_id: $localStorage.currentUser.user_id,
            is_global_admin: $localStorage.currentUser.is_global_admin,
            client_id: $localStorage.currentUser.client_id,
            record_constraints: vm.this_output
        };

        // TODO:
        // if currentUser is global admin then pull down all accounts
        // if currentUser is client seat then pull down accounts by clients or projects
        if ($localStorage.currentUser.is_global_admin) {
            AccountService
                ._get_accounts(_account_query_form)
                .then(
                      function(accounts) {
                        vm.accounts = accounts;
                        // for (var _prop in vm.accounts) {
                        //
                        // }
                      }
                    , function(err) {
                        console.log(err);
                      }
                );
        }
        else {
            ClientProjectAccountService._get_client_project_account(_account_query_form)
                .then(
                      function(results) {
                          var account_ids_list = [];

                          // generate account_ids_list (unique list of account IDs)
                          results.forEach(function(result) {
                              account_ids_list.push(result.tab_account_id);
                          });
                          account_ids_list = account_ids_list.filter(onlyUnique);
                          vm.accounts = [];

                          // generate account_ids_list (unique list of account IDs)
                          account_ids_list.forEach(function(_id) {
                              AccountService
                                  ._get_account(_id)
                                  .then(
                                      function (account) {
                                          vm.accounts.push(account);
                                      }
                                      , function (err) {
                                          console.log(err);
                                      }
                                  );
                          });
                      }
                    , function(err) {
                        console.log(err);
                      }
                )
        }

    }

    function get_account(_id) {
        AccountService
            ._get_account(_id)
            .then(
                function(account) {
                    vm.account = account.data;
                    console.log(vm.account);
                }
                , function(err) {
                    console.log(err);
                }
            )
    }

    this.clear_this_form = function() {
        vm.this_account.screen_name = '';
        vm.this_account.ref_platform_id = {};
    }

    this.repaginate_form = function() {
        console.log(parseInt(vm.this_output.offset));
        console.log(parseInt(vm.this_output.limit));
    }

    this.submit_account_form = function() {
        console.log(vm.this_account);
        AccountService
            ._add_account(vm.this_account)
            .then(
                function(result) {
                    if (result.status == '201') {
                        vm.status = 'New entry saved.';
                        vm.status_class = 'alert alert-success fade in';
                    }
                    else {
                        vm.status = '[ERROR] ' + result.response.status;
                        vm.status_class = 'alert alert-danger fade in';
                        vm.this_account.name = '';
                    }
                }
                , function(err) {
                    vm.status = '[ERROR] ' + err.message;
                    vm.status_class = 'alert alert-danger fade in';
                }
            )
            .then(
                function() {
                    get_accounts();
                    vm.this_account_reset();
                    $timeout(function() {
                        vm.status_reset();
                    }, 5000);
                }
            )
    };

    this.update_account_form = function(_form) {
        AccountService
            ._update_account(_form)
            .then(
                function(result) {
                    if (result.status == '200') {
                        vm.status = 'Existing entry updated.';
                        vm.status_class = 'alert alert-success fade in';
                    }
                }
                , function(err) {
                    vm.status = '[ERROR] ' + err.message;
                    vm.status_class = 'alert alert-danger fade in';
                }
            )
            .then(
                function() {
                    get_accounts();
                    $timeout(function() {
                        vm.status = '';
                        vm.status_class = '';
                    }, 5000);
                }
            )
    };

    this.delete_account = function(_id) {
        AccountService
            ._delete_account(_id)
            .then(
                function(result) {
                    if (result.status == '204') {
                        vm.status = 'Existing entry deleted.';
                        vm.status_class = 'alert alert-success fade in';
                    }
                }
                , function(err) {
                    vm.status = '[ERROR] ' + err.message;
                    vm.status_class = 'alert alert-danger fade in';
                }
            )
            .then(
                function() {
                    get_accounts();
                    $timeout(function() {
                        vm.status = '';
                        vm.status_class = '';
                    }, 5000);
                }
            )
    };
}
