'use strict';

angular
    .module('asb_main')
    .controller('help_controller', _controller);

function _controller($scope, __env, $rootScope) {
    $scope.message;
    var vm = this;

    _init();

    function _init() {
        $rootScope._state_title = 'Help';
    }
}
