'use strict';

angular
    .module('asb_main')
    .controller('index_controller', _controller);

function _controller($scope, __env, $rootScope, $localStorage) {
    $scope.message;
    var vm = this;

    _init();

    function _init() {
        $rootScope._state_title = 'Main Landing Page';
        vm.user_name = $localStorage.currentUser.name
    }
}
