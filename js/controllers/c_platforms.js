'use strict';

angular
    .module('asb_main')
    .controller('platform_controller', _controller);

function _controller($scope, $rootScope, $timeout, $localStorage, PlatformService) {
    $scope.message;
    var vm = this;
    vm.this_platform = {};

    vm.this_platform_reset = function() {
        vm.this_platform.name = '';
    };

    vm.status_reset = function() {
        vm.status = '';
        vm.status_class = '';
    };

    _init();

    function _init() {

        $rootScope._state_title = 'platforms';
        vm.this_platform_reset();
        get_platforms();
    }

    function get_platforms() {
        PlatformService
            ._get_platforms()
            .then(
                function(platforms) {
                    vm.platforms = platforms;

                }
                , function(err) {
                    console.log(err);
                }
            )
    }

    this.submit_platform_form = function() {
        console.log(vm.this_platform);
        PlatformService
            ._add_platform(vm.this_platform)
            .then(
                function(result) {
                    if (result.status == '201') {
                        vm.status = 'New entry saved.';
                        vm.status_class = 'alert alert-success fade in';
                    }
                    else {
                        vm.status = '[ERROR] ' + result.response.status;
                        vm.status_class = 'alert alert-danger fade in';
                        vm.this_platform.name = '';
                    }
                }
                , function(err) {
                    vm.status = '[ERROR] ' + err.message;
                    vm.status_class = 'alert alert-danger fade in';
                }
            )
            .then(
                function() {
                    get_platforms();
                    vm.this_platform_reset();
                    $timeout(function() {
                        vm.status_reset();
                    }, 5000);
                }
            )
    };

    this.update_platform_form = function(_form) {
        PlatformService
            ._update_platform(_form)
            .then(
                function(result) {
                    if (result.status == '200') {
                        vm.status = 'Existing entry updated.';
                        vm.status_class = 'alert alert-success fade in';
                    }
                }
                , function(err) {
                    vm.status = '[ERROR] ' + err.message;
                    vm.status_class = 'alert alert-danger fade in';
                }
            )
            .then(
                function() {
                    get_platforms();
                    $timeout(function() {
                        vm.status = '';
                        vm.status_class = '';
                    }, 5000);
                }
            )
    };

    this.delete_platform = function(_id) {
        PlatformService
            ._delete_platform(_id)
            .then(
                function(result) {
                    if (result.status == '204') {
                        vm.status = 'Existing entry deleted.';
                        vm.status_class = 'alert alert-success fade in';
                    }
                }
                , function(err) {
                    vm.status = '[ERROR] ' + err.message;
                    vm.status_class = 'alert alert-danger fade in';
                }
            )
            .then(
                function() {
                    get_platforms();
                    $timeout(function() {
                        vm.status = '';
                        vm.status_class = '';
                    }, 5000);
                }
            )
    };
}
