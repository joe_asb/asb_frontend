'use strict';

angular
    .module('asb_main')
    .controller('admin_controller', _controller);

function _controller($scope, $rootScope, ClientsService, ClientRolesService) {
    $scope.message;
    var vm = this;

    _init();

    function _init() {
        $rootScope._state_title = 'Account Administration';
        get_clients();
    }


    function get_clients() {
        ClientsService
            ._get_clients()
            .then(
                function(clients) {
                    vm.clients = clients;
                }
                , function(err) {
                    console.log(err);
                }
            )
    }

    function get_client_roles() {
        ClientRolesService
            ._get_client_roles()
            .then(
                function(client_roles) {
                    vm.client_roles = client_roles;
                }
                , function(err) {
                    console.log(err);
                }
            )
    }
}
