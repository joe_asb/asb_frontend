'use strict';

angular
    .module('asb_main')
    .controller('project_controller', _controller);

function _controller($scope, $rootScope, $timeout, $localStorage, ProjectsService, ClientRolesService, ClientSeatsService, ClientsService, PlatformService, CommunityService, CommunityTypeService, $mdDialog, Upload) {
    $scope.message;
    var vm = this;
    vm.this_project = {};

    vm.this_project_reset = function() {
        vm.this_project.name = '';
        vm.this_project.tab_client_id = $localStorage.currentUser.client_id;
        vm.this_project.tab_client_seat_id = $localStorage.currentUser.user_id;
        vm.this_project.alert_all = false;
    };

    vm.status_reset = function() {
        vm.status = '';
        vm.status_class = '';
    };

    _init();

    function _init() {

        $rootScope._state_title = 'Projects';
        vm.this_project_reset();
        get_platforms();
        get_community_types();
        get_client_seat_projects();
    }

    // From https://stackoverflow.com/a/14438954/6554056
    function onlyUnique(value, index, self) {
        return self.indexOf(value) === index;
    }

    function get_community_types() {
        CommunityTypeService
            ._get_community_types()
            .then(
                function(community_types) {
                    vm.community_types = community_types;
                }
                , function(err) {
                    console.log(err);
                }
            )
    }

    function get_platforms() {
        PlatformService
            ._get_platforms()
            .then(
                function(platforms) {
                    vm.platforms = platforms;

                }
                , function(err) {
                    console.log(err);
                }
            )
    }

    // function get_projects() {
    //     ProjectsService
    //         ._get_projects()
    //         .then(
    //             function(projects) {
    //                 vm.projects = projects;
    //                 for (var _prop in vm.projects) {
    //
    //                 }
    //             }
    //             , function(err) {
    //                 console.log(err);
    //             }
    //         )
    // }

    function get_client_seat_projects() {
        ClientRolesService
            ._get_client_role($localStorage.currentUser.role_id)
            .then(
                function(client_role) {
                    vm.client_role = client_role.role;
                }
                , function(err) {
                    console.log(err);
                }
            );

        if (vm.client_role != 'All Client Admin') {
            // Download list of projects for a given user and client
            ClientSeatsService
                ._get_client_seat_projects($localStorage.currentUser.user_id)
                .then(
                    function(projects) {
                        vm.projects = projects.data;
                    }
                    , function(err) {
                        console.log(err);
                    }
                )
        }
        else {
            ProjectsService
                ._get_projects()
                .then(
                    function(projects) {
                        vm.projects = projects.data;
                    }
                    , function(err) {
                        console.log(err);
                    }
                )
        }
    }

    function get_client_project(client_id, project_id) {
        ClientsService
            ._get_client_project(client_id, project_id)
            .then(
                function(project) {
                    vm.project = project.data;
                    console.log(vm.project);
                }
                , function(err) {
                    console.log(err);
                }
            )
    }

    // function get_client_role(role_id) {
    //     ClientRolesService
    //         ._get_client_role(role_id)
    //         .then(
    //             function(client_roles) {
    //                 vm.client_role = client_role;
    //             }
    //             , function(err) {
    //                 console.log(err);
    //             }
    //         )
    // }

    this.submit_project_form = function() {
        console.log(vm.this_project);
        ProjectsService
            ._add_project(vm.this_project)
            .then(
                function(result) {
                    if (result.status == '201') {
                        vm.status = 'New entry saved.';
                        vm.status_class = 'alert alert-success fade in';
                    }
                    else {
                        vm.status = '[ERROR] ' + result.response.status;
                        vm.status_class = 'alert alert-danger fade in';
                        vm.this_project.name = '';
                    }
                }
                , function(err) {
                    vm.status = '[ERROR] ' + err.message;
                    vm.status_class = 'alert alert-danger fade in';
                }
            )
            .then(
                function() {
                    get_client_seat_projects();
                    vm.this_project_reset();
                    $timeout(function() {
                        vm.status_reset();
                    }, 5000);
                }
            )
    };

    this.update_project_form = function(_form) {
        ProjectsService
            ._update_project(_form)
            .then(
                function(result) {
                    if (result.status == '200') {
                        vm.status = 'Existing entry updated.';
                        vm.status_class = 'alert alert-success fade in';
                    }
                }
                , function(err) {
                    vm.status = '[ERROR] ' + err.message;
                    vm.status_class = 'alert alert-danger fade in';
                }
            )
            .then(
                function() {
                    get_client_seat_projects();
                    $timeout(function() {
                        vm.status = '';
                        vm.status_class = '';
                    }, 5000);
                }
            )
    };

    this.delete_project = function(_id) {
        ProjectsService
            ._delete_project(_id)
            .then(
                function(result) {
                    if (result.status == '204') {
                        vm.status = 'Existing entry deleted.';
                        vm.status_class = 'alert alert-success fade in';
                    }
                }
                , function(err) {
                    vm.status = '[ERROR] ' + err.message;
                    vm.status_class = 'alert alert-danger fade in';
                }
            )
            .then(
                function() {
                    get_client_seat_projects();
                    $timeout(function() {
                        vm.status = '';
                        vm.status_class = '';
                    }, 5000);
                }
            );
    };

    this.manage_accounts = function(_project, ev) {
        // ClientsService
        //     ._get_client_project(vm.this_project.tab_client_id, _project.id)
        //     .then(
        //         function(project) {
                    // vm.project = project.data;
                    console.log(_project);
                    $mdDialog
                        .show({
                            controller: project_accounts_dialog_ctrl,
                            templateUrl: '/html/view_project_accounts.html',
                            parent: angular.element(document.body), // by default
                            targetEvent: ev,
                            clickOutsideToClose:true,
                            controllerAs: '_details',
                            // fullscreen: $scope.customFullscreen,
                            locals: {
                                _this_project: _project
                            }
                        })
                        .then(
                            function(x) {

                            }, function() {
                            console.log('Account dialog cancelled from outside.');
                        });
                // }
                // , function(err) {
                //     console.log(err);
                // }
            // )
    };

    this.manage_communities = function(_project, ev) {
        console.log(_project);
        $mdDialog
            .show({
                controller: project_communities_dialog_ctrl,
                templateUrl: '/html/view_project_communities.html',
                parent: angular.element(document.body), // by default
                targetEvent: ev,
                clickOutsideToClose:true,
                controllerAs: '_details',
                // fullscreen: $scope.customFullscreen,
                locals: {
                    _this_project: _project
                }
            })
            .then(
                function(x) {

                }, function() {
                    console.log('Community dialog cancelled from outside.');
                });
    //     // ClientsService
    //     //     ._get_client_project(vm.this_project.tab_client_id, _project.id)
    //     //     .then(
    //     //         function(project) {
    //     //             vm.project = project.data;
    //                 console.log(_project);
    //                 $mdDialog.show({
    //                     controller: project_communities_dialog_ctrl,
    //                     templateUrl: '/html/view_project_communities.html',
    //                     parent: angular.element(document.body), // by default
    //                     targetEvent: ev,
    //                     clickOutsideToClose:false,
    //                     controllerAs: '_details',
    //                     // fullscreen: $scope.customFullscreen,
    //                     locals: {
    //                         _this_project: _project,
    //                         _project_accounts: vm.accounts,
    //                         _these_community_types: vm.community_types
    //                     }
    //                 }).then(function(x) {
    //
    //                 }, function() {
    //                     console.log('Community dialog cancelled from outside.');
    //                 })
    //         //     }
    //         //     , function(err) {
    //         //         console.log(err);
    //         //     }
    //         // )
    };

    function project_accounts_dialog_ctrl ($scope, $mdDialog, _this_project, AccountService, AccountTypeService, ClientProjectAccountService) {
        // console.log('Details controller!!!');
        $scope._project_id = _this_project.id;
        $scope._project_name = _this_project.name;
        $scope._platform_id = _this_project.platform.id;
        $scope._project = _this_project;
        $scope._project_accounts = [];
        $scope._account_types = [];
        $scope._new_account = {};

        _init();

        function _init() {
            get_account_types();
            get_project_accounts();
        }

        $scope.cancel_dialog = function() {
            console.log("Push account x button.");
            $mdDialog.cancel();
        };

        $scope.clear_account_form = function() {
            $scope._new_account = {
                screen_name: "",
                ref_account_type_id: 0
            };
        };

        function get_account_types() {
            $scope._account_types = [];
            AccountTypeService
                ._get_account_types()
                .then(
                    function(results) {
                        // generate _account_types (list of account types)
                        results.forEach(function(result) {
                            $scope._account_types.push({id: result.id, typename: result.typename});
                        });
                    },
                    function(error) {
                        console.log(error);
                    }
                );
        }

        // TODO: May want to use user search for autocomplete
        // https://material.angularjs.org/latest/demo/autocomplete
        // https://developer.twitter.com/en/docs/accounts-and-users/follow-search-get-users/api-reference/get-users-search
        // 900 req / 15 min
        $scope.add_project_account = function(_project) {
            var form_data_account = {
                  screen_name: $scope._new_account.screen_name
                , platform: _project.platform.name
            };

            // Add account
            AccountService
                ._add_account(form_data_account)
                .then(
                    function(new_account_results) {
                        var form_data_client_project_account = {
                              ref_account_type_id: $scope._new_account.ref_account_type_id
                            , tab_project_id: _project.id
                            , tab_account_id: new_account_results.id
                        };

                        // add account association with project
                        ClientProjectAccountService
                            ._add_client_project_account(form_data_client_project_account)
                            .then(
                                function(assoc_results) {
                                    // Display result status and clear new entry form
                                    get_project_accounts();
                                    $scope.clear_account_form();
                                    $scope.status = 'Account Added to Project.';
                                    $scope.status_class = 'alert alert-success fade in';
                                },
                                function(err) {
                                    $scope.status = '[ERROR] ' + err.message;
                                    $scope.status_class = 'alert alert-danger fade in';
                                }
                            )
                            .then(
                                function() {
                                    $timeout(function() {
                                        $scope.status = "";
                                        $scope.status_class = "";
                                    }, 5000);
                                }
                            );
                    },
                    function(err) {
                        console.log(err);
                    }
                );
        };

        // TODO: May want to use user search for autocomplete
        // https://material.angularjs.org/latest/demo/autocomplete
        // https://developer.twitter.com/en/docs/accounts-and-users/follow-search-get-users/api-reference/get-users-search
        // 900 req / 15 min
        $scope.update_project_account_relation = function(_project, _account) {
            var _account_query_form = {
                project_id: _project.id,
                account_id: _account.id
            };

            // find relation ID
            ClientProjectAccountService
                ._get_client_project_account(_account_query_form)
                .then(
                    function(assoc_result) {
                        // TODO: find out why running tab_client_project_accounts.json GET with account and project IDs won't return single item - JWT 22 Jan 2018
                        var client_project_account_id = assoc_result[0].id;

                        // Add new account using user provided screen_name
                        var account_data = {
                            screen_name: _account.screen_name,
                            platform: _project.platform.name
                        };

                        AccountService
                            ._add_account(account_data)
                            .then(
                                function (new_account_result) {
                                    var updated_client_project_account = {
                                        id: client_project_account_id,
                                        tab_account_id: new_account_result.id
                                    };

                                    // update relation of given ID with newly created account ID
                                    ClientProjectAccountService
                                        ._update_client_project_account(updated_client_project_account)
                                        .then(
                                            function(result) {
                                                get_project_accounts();
                                                $scope.status = 'Account Updated in Project.';
                                                $scope.status_class = 'alert alert-success fade in';
                                            },
                                            function (err) {
                                                $scope.status = '[ERROR] ' + err.message;
                                                $scope.status_class = 'alert alert-danger fade in';
                                            }
                                        );

                                },
                                function (err) {
                                    $scope.status = '[ERROR] ' + err.message;
                                    $scope.status_class = 'alert alert-danger fade in';
                                }
                            );
                    },
                    function(err) {
                        $scope.status = '[ERROR] ' + err.message;
                        $scope.status_class = 'alert alert-danger fade in';
                    }
                )
                .then(
                    function() {
                        $timeout(function() {
                            $scope.status = "";
                            $scope.status_class = "";
                        }, 5000);
                    }
                );
        };

        $scope.delete_project_account_relation = function(project_id, account_id) {
            var _account_query_form = {
                project_id: _this_project.id,
                account_id: account_id
            };
            // find relation ID
            ClientProjectAccountService
                ._get_client_project_account(_account_query_form)
                .then(
                    function(result) {
                        // delete relation
                        // TODO: find out why running tab_client_project_accounts.json GET with account and project IDs won't return single item - JWT 22 Jan 2018
                        var client_project_account_id = result[0].id;
                        ClientProjectAccountService
                            ._delete_client_project_account(client_project_account_id)
                            .then(
                                function(result) {
                                    get_project_accounts();
                                    $scope.status = 'Account Removed from Project.';
                                    $scope.status_class = 'alert alert-success fade in';
                                },
                                function(error) {
                                    $scope.status = '[ERROR] ' + err.message;
                                    $scope.status_class = 'alert alert-danger fade in';
                                }
                            );
                    },
                    function(err) {
                        $scope.status = '[ERROR] ' + err.message;
                        $scope.status_class = 'alert alert-danger fade in';
                    }
                )
                .then(
                    function() {
                        $timeout(function() {
                            $scope.status = "";
                            $scope.status_class = "";
                        }, 5000);
                    }
                );
        };

        function get_project_accounts() {

            // NOTE: the 0 parameter means to exclude accounts marked as `list_data`
            AccountService._get_accounts_by_project(_this_project.id, 0)
                .then(
                    function (accounts) {
                        $scope._project_accounts = accounts;
                    },
                    function (err) {
                        console.log(err);
                    }
                );
        }
    }

    function project_communities_dialog_ctrl ($scope, $mdDialog, _this_project, ClientProjectCommunityService) {
        console.log('Details controller!!!');
        // $scope._form = {};
        // $scope._form.this_file = '';
        // $scope._form._project_id = _this_project.id;
        // $scope._form._platform_id = _this_project.platform.id;
        // $scope._form._project = _this_project;
        // console.log(_this_project);
        $scope._project = _this_project;
        $scope._platform_id = _this_project.platform.id;
        $scope._project_communities = [];
        $scope._new_community = {};

            _init();

            function _init() {
                get_project_communities();
            }

        $scope.cancel_dialog = function() {
            console.log("Push account x button.");
            $mdDialog.cancel();
        };

        $scope.clear_community_form = function() {
            $scope._new_community = {
                screen_name: "",
                ref_account_type_id: 0
            };
        };

        function get_project_communities() {
            $scope._project_communities = [];
            var _community_query_form = {
                project_id: _this_project.id
                // record_constraints: vm.this_output   // should need it once/assuming we start paginating project accounts
            };

            ClientProjectCommunityService
                ._get_client_project_community(_community_query_form)
                .then(
                    function(results) {
                        console.log('[610] Got here');
                    }
                );

            // ClientProjectAccountService._get_client_project_account(_account_query_form)
            //     .then(
            //         function(results) {
            //             var account_ids_list = {};
            //
            //             // generate account_ids_list (unique list of account IDs)
            //             results.forEach(function(result) {
            //                 account_ids_list[result.tab_account_id] = result.ref_account_type_id;
            //             });
            //             // account_ids_list = account_ids_list.filter(onlyUnique);
            //             vm.accounts = [];
            //
            //             // generate account_ids_list (unique list of account IDs)
            //             Object.keys(account_ids_list).forEach(function(_id) {
            //                 AccountService
            //                     ._get_account(_id)
            //                     .then(
            //                         function (account) {
            //                             account.ref_account_type_id = account_ids_list[_id];    // append project-specific account type to account object
            //                             $scope._project_accounts.push(account);
            //                         },
            //                         function (err) {
            //                             console.log(err);
            //                         }
            //                     );
            //             });
            //         }
            //     );

        }

    //
    //     $scope.add_community = function(_project) {
    //         console.log(_project);
    //     };
    //
    //     function get_community_accounts(_community) {
    //         console.log('[565] community: ', _community);
    //     }
    //
    //     // $scope.import_community_file = function() {
    //     //     // use _this_project to extract project ID and platform ID
    //     //     // TODO: submit $scope._form into API POST method; file is $scope._form.this_file
    //     // };
    //
    }

    this.submit_community_form = function() {
        console.log(vm.this_community);
        CommunityService
            ._add_community(vm.this_community)
            .then(
                function(result) {
                    if (result.status == '201') {
                        vm.status = 'New entry saved.';
                        vm.status_class = 'alert alert-success fade in';
                    }
                    else {
                        vm.status = '[ERROR] ' + result.response.status;
                        vm.status_class = 'alert alert-danger fade in';
                        vm.this_account.name = '';
                    }
                }
                , function(err) {
                    vm.status = '[ERROR] ' + err.message;
                    vm.status_class = 'alert alert-danger fade in';
                }
            )
            .then(
                function() {
                    get_communities();
                    vm.this_community_reset();
                    $timeout(function() {
                        vm.status_reset();
                    }, 5000);
                }
            )
    };

    this.update_community_form = function(_form) {
        CommunityService
            ._update_community(_form)
            .then(
                function(result) {
                    if (result.status == '200') {
                        vm.status = 'Existing entry updated.';
                        vm.status_class = 'alert alert-success fade in';
                    }
                }
                , function(err) {
                    vm.status = '[ERROR] ' + err.message;
                    vm.status_class = 'alert alert-danger fade in';
                }
            )
            .then(
                function() {
                    get_accounts();
                    $timeout(function() {
                        vm.status = '';
                        vm.status_class = '';
                    }, 5000);
                }
            )
    };

    this.delete_community = function(_id) {
        CommunityService
            ._delete_community(_id)
            .then(
                function(result) {
                    if (result.status == '204') {
                        vm.status = 'Existing entry deleted.';
                        vm.status_class = 'alert alert-success fade in';
                    }
                }
                , function(err) {
                    vm.status = '[ERROR] ' + err.message;
                    vm.status_class = 'alert alert-danger fade in';
                }
            )
            .then(
                function() {
                    get_accounts();
                    $timeout(function() {
                        vm.status = '';
                        vm.status_class = '';
                    }, 5000);
                }
            )
    };
}
