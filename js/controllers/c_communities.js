"use strict";

angular
    .module("asb_main")
    .controller("community_controller", _controller);

function _controller($scope, $rootScope, $timeout, $localStorage, PlatformService, ClientsService, CommunityService, CommunityAccountService, ClientProjectCommunityService, CommunityTypeService, ClientRolesService, ClientSeatsService, ProjectsService, ProjPlatformService, $mdDialog) {
    $scope.message;
    var vm = this;
    vm.this_community = {};

    vm.clear_this_form = function() {
        vm.this_community.role_id = $localStorage.currentUser.role_id;
        vm.this_community.name = "";
        vm.this_community.project_id = 0;
        // vm.this_community.ref_platform_id = -1;
    };

    vm.status_reset = function() {
        vm.status = "";
        vm.status_class = "";
    };

    _init();

    function _init() {
        $rootScope._state_title = 'Communities';
        get_communities();
        // get_community_types();
        get_client_seat_projects();
        get_platforms();
        is_global_admin();
    }

    function get_platforms() {
        PlatformService
            ._get_platforms()
            .then(
                function(platforms) {
                    vm.platforms = platforms;

                },
                function(err) {
                    console.log(err);
                }
            );
    }

    // TODO: Need to refactor - not enough code reuse - JWT 23 Oct 2017
    function get_communities() {
        ClientRolesService
            ._get_client_role($localStorage.currentUser.role_id)
            .then(
                function(client_role) {
                    vm.client_role = client_role.role;

                    // get communities list for non-admin user
                    if (vm.client_role !== "All Client Admin") {
                        // Download list of projects for a given user and client
                        ClientSeatsService
                            ._get_client_seat_projects($localStorage.currentUser.user_id)
                            .then(
                                function(projects) {
                                    if (projects.data.length > 0) {
                                        vm.projects = projects.data;
                                        // Get list of communities for a given project

                                        // TODO: may want to refactor the section below using CommunityService._get_project_communities
                                        // TODO: since project can have collection of communities
                                        // TODO: and maybe have the communities be grouped by project - JWT 16 Apr 2018
                                        ClientProjectCommunityService
                                            ._get_client_project_community(vm.projects)
                                            .then(
                                                function(results) {
                                                    CommunityService
                                                        ._get_communities(results)
                                                        .then(
                                                            function(communities) {
                                                                vm.communities = communities;
                                                            },
                                                            function(err) {
                                                                console.log(err);
                                                            }
                                                        )
                                                    ;

                                                }
                                            );
                                    }
                                },
                                function(err) {
                                    console.log(err);
                                }
                            );

                        // TODO: Download list of global communities created by Afferent admin
                        ClientsService
                            ._get_client_by_name("Afferent")
                            .then(
                                function(client_results) {
                                    var params = {
                                        tab_client_id: client_results.data[0].id,
                                        name: "Global"
                                    };
                                    ProjectsService
                                        ._get_project_by_name(params)
                                        .then(
                                            function(project_results) {
                                                // get ID for global project
                                                vm.global_project_id = project_results[0];
                                                // use global project ID to get communities
                                                CommunityService
                                                    ._get_project_communities(project_results[0].id)
                                                    .then(
                                                        function(communities) {
                                                            vm.global_communities = communities;
                                                        }
                                                    );
                                            }
                                        );
                                }
                            );
                    }
                    else {
                        // get communities list for admin user
                        ProjectsService
                            ._get_projects()
                            .then(
                                function(projects) {
                                    vm.projects = projects.data;
                                    ClientProjectCommunityService
                                        ._get_client_project_community(vm.projects)
                                        .then(
                                            function(results) {

                                                CommunityService
                                                    ._get_communities(results)
                                                    .then(
                                                        function(communities) {
                                                            vm.communities = communities;
                                                            for (var _prop in vm.communities) {
                                                                vm.clear_this_form();
                                                            }
                                                        }
                                                        , function(err) {
                                                            console.log(err);
                                                        }
                                                    )
                                            }
                                        );
                                }
                                , function(err) {
                                    console.log(err);
                                }
                            )
                    }
                }
                , function(err) {
                    console.log(err);
                }
            )
    }

    // function get_community_types() {
    //     CommunityTypeService
    //         ._get_community_types()
    //         .then(
    //             function(community_types) {
    //                 vm.community_types = community_types;
    //             }
    //             , function(err) {
    //                 console.log(err);
    //             }
    //         )
    // }


    function is_global_admin() {
        ClientRolesService
            ._get_client_role($localStorage.currentUser.role_id)
            .then(
                function(client_role) {
                    vm.client_role = client_role.role;
                    $localStorage.currentUser.is_global_admin = (vm.client_role == 'All Client Admin');
                    $scope.is_global_admin = (vm.client_role == 'All Client Admin');

                    // var proj_platform_params = {
                    //       name: 'Global'
                    //     , tab_client_seat: $localStorage.currentUser.user_id
                    // }
                    // ProjPlatformService
                    //     ._get_projplatforms(proj_platform_params)
                    //     .then(
                    //         function(projplatforms) {
                    //             vm.projplatforms = projplatforms;
                    //         }
                    //     )
                }
                , function(err) {
                    console.log(err);
                }
            );
    }


    function get_client_seat_projects() {
        ClientRolesService
            ._get_client_role($localStorage.currentUser.role_id)
            .then(
                function(client_role) {
                    vm.client_role = client_role.role;
                }
                , function(err) {
                    console.log(err);
                }
            );

        if (vm.client_role != 'All Client Admin') {
            // Download list of projects for a given user and client
            ClientSeatsService
                ._get_client_seat_projects($localStorage.currentUser.user_id)
                .then(
                    function(projects) {
                        vm.projects = projects.data;
                    }
                    , function(err) {
                        console.log(err);
                    }
                )
        }
        else {
            ProjectsService
                ._get_projects()
                .then(
                    function(projects) {
                        vm.projects = projects.data;
                    }
                    , function(err) {
                        console.log(err);
                    }
                )
        }
    }

    function get_community(_id) {
        CommunityService
            ._get_community(_id)
            .then(
                function(community) {
                    vm.community = community.data;
                    console.log(vm.community);
                }
                , function(err) {
                    console.log(err);
                }
            )
    }

    // 1. Add community
    // 2. Query tab_projects with userid and "global" to get project_ids
    // 3. Get project_id from tab_project_platforms that match platform
    // 4. Add relation to tab_client_project_communities
    this.submit_community_form = function() {
        vm.this_community.role_id = $localStorage.currentUser.role_id;
        
        // Step 1
        CommunityService
            ._add_community(vm.this_community)
            .then(
                function(result) {
                    if (result.status == '201') {
                        // new client_project_community entry also requires a ref_platform_id value, currently handled in server side.
                        // may want to refactor this and move ref_platform_id lookup to Javascript side - JWT 23 Oct 2017

                        if ($localStorage.currentUser.is_global_admin) {
                            // Retrieve ID for Global project
                            var _project_query_form = {
                                  name: 'Global'
                                , tab_client_seat_id: $localStorage.currentUser.user_id
                            };
                            // TODO: _get_projects() should be refactored to include status in results
                            // Step 2
                            ProjectsService
                                ._get_projects(_project_query_form)
                                .then(
                                    function(r_projects) {
                                        var project_ids = [];

                                        r_projects.map(function(item) { project_ids.push(item.id); });

                                        var _project_platforms_form = {
                                              ref_platform_id: parseInt(vm.this_community.platform_id)
                                            , tab_project_ids: project_ids
                                        };
                                        // Step 3
                                        ProjPlatformService
                                            ._get_projplatforms(_project_platforms_form)
                                            .then(
                                                function(r_projplatforms) {
                                                    if (r_projplatforms.status == '200') {
                                                        var _client_project_communities_form = {
                                                              tab_project_id: r_projplatforms.data[0].tab_project_id
                                                            , tab_community_id: result.data.id
                                                            , ref_platform_id: parseInt(vm.this_community.platform_id)
                                                            , file: vm.this_community.file
                                                        }

                                                        // Step 4
                                                        ClientProjectCommunityService
                                                            ._add_client_project_community(_client_project_communities_form)
                                                            .then(
                                                                function(child_result) {
                                                                    if (child_result.status == '201') {
                                                                        vm.status = 'New entry saved.';
                                                                        vm.status_class = 'alert alert-success fade in';
                                                                        get_client_seat_projects();
                                                                    }
                                                                }
                                                            )
                                                    }
                                                    else {
                                                        vm.status = '[ERROR] ' + result.response.status;
                                                        vm.status_class = 'alert alert-danger fade in';
                                                        vm.this_community.name = '';
                                                    }

                                                }
                                            );

                                    }

                                )
                        }
                        else {
                            var _this_form = {
                                  tab_project_id: parseInt(vm.this_community.project_id)
                                , tab_community_id: result.data.id
                                , file: vm.this_community.file
                            };

                            ClientProjectCommunityService
                                ._add_client_project_community(_this_form)
                                .then(
                                    function(child_result) {
                                        vm.status = 'New entry saved.';
                                        vm.status_class = 'alert alert-success fade in';
                                        get_client_seat_projects();
                                    }
                                )
                        }

                    }
                    else {
                        vm.status = '[ERROR] ' + result.response.status;
                        vm.status_class = 'alert alert-danger fade in';
                        vm.this_community.name = '';
                    }
                }
                , function(err) {
                    vm.status = '[ERROR] ' + err.message;
                    vm.status_class = 'alert alert-danger fade in';
                }
            )
            .then(
                function() {
                    get_communities();
                    vm.this_community_reset;
                    $timeout(function() {
                        vm.status_reset();
                    }, 5000);
                }
            )
    };

    // 1. Delete relation to tab_client_project_communities
    // 2. Delete community
    this.delete_community = function(_id) {
        var client_project_community = {
              client: $localStorage.currentUser.client_id
            , project_id: -1
            , community_id: _id
        };
        console.log(client_project_community);
        ClientProjectCommunityService
            ._delete_client_project_community(1);

        // CommunityService
        //     ._delete_community(_id)
        //     .then(
        //         function(result) {
        //             if (result.status == '204') {
        //                 vm.status = 'Existing entry deleted.';
        //                 vm.status_class = 'alert alert-success fade in';
        //             }
        //         }
        //         , function(err) {
        //             vm.status = '[ERROR] ' + err.message;
        //             vm.status_class = 'alert alert-danger fade in';
        //         }
        //     )
        //     .then(
        //         function() {
        //             get_communities();
        //             $timeout(function() {
        //                 vm.status = '';
        //                 vm.status_class = '';
        //             }, 5000);
        //         }
        //     )
    };

    this.manage_community_accounts = function(_community, ev) {
        console.log(_community);
        $mdDialog.show({
            controller: community_accounts_dialog_ctrl,
            templateUrl: '/html/view_community_accounts.html',
            parent: angular.element(document.body), // by default
            targetEvent: ev,
            clickOutsideToClose:false,
            controllerAs: '_details',
            // fullscreen: $scope.customFullscreen,
            locals: {
                _this_community: _community
            }
        }).then(function(x) {

        }, function() {
            console.log('Communities dialog cancelled.');
        })
    };

    this.update_community_form = function(_form) {
        console.log(_form);
        CommunityService
            ._update_community(_form)
            .then(
                function(result) {
                    if (result.status == '200') {
                        vm.status = 'Existing entry updated.';
                        vm.status_class = 'alert alert-success fade in';
                    }
                }
                , function(err) {
                    vm.status = '[ERROR] ' + err.message;
                    vm.status_class = 'alert alert-danger fade in';
                }
            )
            .then(
                function() {
                    get_communities();
                    $timeout(function() {
                        vm.status = '';
                        vm.status_class = '';
                    }, 5000);
                }
            )
    };

    // Export community accounts to CSV file
    this.export_community_to_csv = function(_community, limit) {
      CommunityService
          ._export_community(_community.id, limit, 'csv')
          .then(
              function(result) {
                  var csvContent = "data:text/csv;charset=utf-8,";

                  // data contains the CSV data
                  csvContent += result;
                  var encodedUri = encodeURI(csvContent);
                  var link = document.createElement("a");
                  link.setAttribute("href", encodedUri);
                  link.setAttribute("download", "community_export.csv");

                  // This will download the data file named "community_export.csv".
                  link.click();
              }
          )
    };

    function community_accounts_dialog_ctrl ($scope, $mdDialog, _this_community, CommunityAccountService) {
        // console.log('Details controller!!!');
        $scope._form = {};
        $scope._form.this_file = '';
        $scope._this_community = _this_community;
        // $scope._this_community.import = {};

        _init();

        function _init() {
            // get_community_accounts();
        }

        // NOTE: commented out for now since communities list details button commented out, no need for account details in modal window - JWT 29 Jan 2018
        // function get_community_accounts() {
        //     var form_data = {
        //         tab_community_ids: [$scope._this_community.id]
        //     };
        //
        //     CommunityAccountService
        //         ._get_community_accounts(form_data)
        //         .then(
        //             function(community_accounts_results) {
        //                 var account_ids = [];
        //                 community_accounts_results.forEach(function(community_accounts_result) {
        //                     account_ids.push(community_accounts_result.id);
        //                 });
        //                 console.log(account_ids);
        //                 AccountService
        //                     ._get_accounts(account_ids)
        //                     .then()
        //             },
        //             function(err) {
        //                 vm.status = '[ERROR] ' + err.message;
        //                 vm.status_class = 'alert alert-danger fade in';
        //             }
        //
        //         )
        //         .then(
        //             function() {
        //                 get_communities();
        //                 $timeout(function() {
        //                     vm.status = "";
        //                     vm.status_class = "";
        //                 }, 5000);
        //             }
        //         );
        // }

        // $scope.import_community = function(_form) {
        //     // use _this_project to extract project ID and platform ID
        //     console.log(_form);
        //     CommunityService
        //         ._import_community(_form)
        //         .then(
        //             function(result) {
        //                 if (result.status == '200') {
        //                     vm.status = 'Existing entry updated.';
        //                     vm.status_class = 'alert alert-success fade in';
        //                 }
        //             }
        //             , function(err) {
        //                 vm.status = '[ERROR] ' + err.message;
        //                 vm.status_class = 'alert alert-danger fade in';
        //             }
        //         )
        //         .then(
        //             function() {
        //                 get_communities();
        //                 $timeout(function() {
        //                     vm.status = '';
        //                     vm.status_class = '';
        //                 }, 5000);
        //             }
        //         )
        // };

        $scope.cancel_dialog = function() {
            $mdDialog.cancel();
        };

    }
}
