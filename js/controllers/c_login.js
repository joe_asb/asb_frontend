'use strict';

angular
    .module('asb_main')
    .controller('login_controller', _controller);

function _controller($scope, __env, $rootScope, $location, AuthenticationService) {
    $scope.message;
    var vm = this;
    vm.login = login;

    _init();

    function _init() {
        vm.loading = false;
        $rootScope._state_title = 'Login';
        AuthenticationService.Logout();
    }

    function login() {
        vm.loading = true;
        AuthenticationService.Login(vm._email, vm._password, function (result) {
            if (result === true) {
                $location.path('/');
            } else {
                vm.error = 'Username or password is incorrect';
                vm.loading = false;
            }
        });
    };
}
