'use strict';

angular
    .module('asb_main')
    .controller('community_controller', _controller);

function _controller($scope, $rootScope, $timeout, $localStorage, PlatformService, CommunityService, CommunityTypeService, $mdDialog) {
    $scope.message;
    var vm = this;
    vm.this_community = {};

    vm.this_community_reset = function() {
        vm.this_community.screen_name = '';
        vm.this_community.ref_platform_id = -1;
    };

    vm.status_reset = function() {
        vm.status = '';
        vm.status_class = '';
    };

    _init();

    function _init() {

        $rootScope._state_title = 'Communities';
        vm.this_community_reset();
        get_communities();
        get_community_types()
        get_platforms();
    }
    this.submit_community_form = function() {
        console.log(vm.this_community);
        CommunityService
            ._add_community(vm.this_community)
            .then(
                function(result) {
                    if (result.status == '201') {
                        vm.status = 'New entry saved.';
                        vm.status_class = 'alert alert-success fade in';
                    }
                    else {
                        vm.status = '[ERROR] ' + result.response.status;
                        vm.status_class = 'alert alert-danger fade in';
                        vm.this_community.name = '';
                    }
                }
                , function(err) {
                    vm.status = '[ERROR] ' + err.message;
                    vm.status_class = 'alert alert-danger fade in';
                }
            )
            .then(
                function() {
                    get_communities();
                    vm.this_community_reset();
                    $timeout(function() {
                        vm.status_reset();
                    }, 5000);
                }
            )
    };

    this.delete_community = function(_id) {
        CommunityService
            ._delete_community(_id)
            .then(
                function(result) {
                    if (result.status == '204') {
                        vm.status = 'Existing entry deleted.';
                        vm.status_class = 'alert alert-success fade in';
                    }
                }
                , function(err) {
                    vm.status = '[ERROR] ' + err.message;
                    vm.status_class = 'alert alert-danger fade in';
                }
            )
            .then(
                function() {
                    get_communities();
                    $timeout(function() {
                        vm.status = '';
                        vm.status_class = '';
                    }, 5000);
                }
            )
    };

    this.manage_community_accounts = function(_community, ev) {
        console.log(_community);
        $mdDialog.show({
            controller: community_accounts_dialog_ctrl,
            templateUrl: '/html/view_community_accounts.html',
            parent: angular.element(document.body), // by default
            targetEvent: ev,
            clickOutsideToClose:false,
            controllerAs: '_details',
            // fullscreen: $scope.customFullscreen,
            locals: {
                _this_community: _community
            }
        }).then(function(x) {

        }, function() {
            console.log('Communities dialog cancelled.');
        })
    };

    this.update_community_form = function(_form) {
        console.log(_form);
        CommunityService
            ._update_community(_form)
            .then(
                function(result) {
                    if (result.status == '200') {
                        vm.status = 'Existing entry updated.';
                        vm.status_class = 'alert alert-success fade in';
                    }
                }
                , function(err) {
                    vm.status = '[ERROR] ' + err.message;
                    vm.status_class = 'alert alert-danger fade in';
                }
            )
            .then(
                function() {
                    get_communities();
                    $timeout(function() {
                        vm.status = '';
                        vm.status_class = '';
                    }, 5000);
                }
            )
    };

    function community_accounts_dialog_ctrl ($scope, $mdDialog, _this_community) {
        // console.log('Details controller!!!');
        $scope._form = {};
        $scope._form.this_file = '';
        $scope._this_community = _this_community;
        // $scope._this_community.import = {};

        $scope.import_community = function(_form) {
            // use _this_project to extract project ID and platform ID
            console.log(_form);
            CommunityService
                ._import_community(_form)
                .then(
                    function(result) {
                        if (result.status == '200') {
                            vm.status = 'Existing entry updated.';
                            vm.status_class = 'alert alert-success fade in';
                        }
                    }
                    , function(err) {
                        vm.status = '[ERROR] ' + err.message;
                        vm.status_class = 'alert alert-danger fade in';
                    }
                )
                .then(
                    function() {
                        get_communities();
                        $timeout(function() {
                            vm.status = '';
                            vm.status_class = '';
                        }, 5000);
                    }
                )
        };

        $scope.cancel_dialog = function() {
            $mdDialog.cancel();
        };

    }
}
