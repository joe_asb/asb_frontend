'use strict';

angular
    .module('asb_main')
    .controller('client_controller', _controller);

function _controller($scope, $rootScope, $timeout, ClientsService, ClientRolesService) {
    $scope.message;
    var vm = this;

    _init();

    function _init() {
        $rootScope._state_title = 'Account Administration';
        get_clients();
        // vm.this_client = {'mode': 'insert'};
    }

    function get_clients() {
        ClientsService
            ._get_clients()
            .then(
                function(result) {
                    vm.clients = result.data;
                }
                , function(err) {
                    console.log(err);
                }
            )
    }

    function get_client_roles() {
        ClientRolesService
            ._get_client_roles()
            .then(
                function(client_roles) {
                    vm.client_roles = client_roles;
                }
                , function(err) {
                    console.log(err);
                }
            )
    }

    this.clear_this_form = function() {
        vm.this_client = {};
    }

    this.submit_form = function() {
        ClientsService
            ._add_client(vm.this_client)
            .then(
                function(result) {
                    if (result.status == '201') {
                        vm.status = 'New entry saved.';
                        vm.status_class = 'alert alert-success fade in';
                    }
                }
                , function(err) {
                    vm.status = '[ERROR] ' + err.message;
                    vm.status_class = 'alert alert-danger fade in';
                }
            )
            .then(
                function() {
                    vm.this_client = {};
                    get_clients();
                    $timeout(function() {
                        vm.status = '';
                        vm.status_class = '';
                    }, 5000);
                }
            )
    };

    this.update_form = function(_form) {
        ClientsService
            ._update_client(_form)
            .then(
                function(result) {
                    if (result.status == '200') {
                        vm.status = 'Existing entry updated.';
                        vm.status_class = 'alert alert-success fade in';
                    }
                }
                , function(err) {
                    vm.status = '[ERROR] ' + err.message;
                    vm.status_class = 'alert alert-danger fade in';
                }
            )
            .then(
                function() {
                    get_clients();
                    $timeout(function() {
                        vm.status = '';
                        vm.status_class = '';
                    }, 5000);
                }
            )
    };

    this.delete_client = function(_id) {
        ClientsService
            ._delete_client(_id)
            .then(
                function(result) {
                    if (result.status == '204') {
                        vm.status = 'Existing entry deleted.';
                        vm.status_class = 'alert alert-success fade in';
                    }
                }
                , function(err) {
                    vm.status = '[ERROR] ' + err.message;
                    vm.status_class = 'alert alert-danger fade in';
                }
            )
            .then(
                function() {
                    get_clients();
                    $timeout(function() {
                        vm.status = '';
                        vm.status_class = '';
                    }, 5000);
                }
            )
    };
}
