'use strict';

angular
    .module('asb_main')
    .controller('seat_controller', _controller);

function _controller($scope, $rootScope, $timeout, ClientsService, ClientSeatsService, ClientRolesService) {
    $scope.message;
    var vm = this;

    _init();

    function _init() {
        $rootScope._state_title = 'Account Administration';
        get_clients();
        get_client_seats();
        get_client_roles();
    }


    function get_clients() {
        ClientsService
            ._get_clients()
            .then(
                function(clients) {
                    vm.clients = clients.data;
                }
                , function(err) {
                    console.log(err);
                }
            )
    }

    function get_client_seats() {
        ClientSeatsService
            ._get_client_seats()
            .then(
                function(seats) {
                    vm.seats = seats;
                }
                , function(err) {
                    console.log(err);
                }
            )
    }

    function get_client_roles() {
        ClientRolesService
            ._get_client_roles()
            .then(
                function(client_roles) {
                    vm.client_roles = client_roles;
                }
                , function(err) {
                    console.log(err);
                }
            )
    }

    this.submit_form = function() {
        ClientSeatsService
            ._add_client_seat(vm.this_seat)
            .then(
                function(result) {
                    if (result.status == '201') {
                        vm.status = 'New entry saved.';
                        vm.status_class = 'alert alert-success fade in';
                        vm.this_seat = {};
                    }
                }
                , function(err) {
                    vm.status = '[ERROR] ' + err.message;
                    vm.status_class = 'alert alert-danger fade in';
                }
            )
            .then(
                function() {
                    vm.this_client = {};
                    get_client_seats();
                    $timeout(function() {
                        vm.status = '';
                        vm.status_class = '';
                    }, 5000);
                }
            )
    }

    this.update_form = function(_form) {
        ClientSeatsService
            ._update_client_seat(_form)
            .then(
                function(result) {
                    if (result.status == '200') {
                        vm.status = 'Existing entry updated.';
                        vm.status_class = 'alert alert-success fade in';
                    }
                }
                , function(err) {
                    vm.status = '[ERROR] ' + err.message;
                    vm.status_class = 'alert alert-danger fade in';
                }
            )
            .then(
                function() {
                    get_client_seats();
                    $timeout(function() {
                        vm.status = '';
                        vm.status_class = '';
                    }, 5000);
                }
            )
    };

    this.delete_client_seat = function(_id) {
        ClientSeatsService
            ._delete_client_seat(_id)
            .then(
                function(result) {
                    if (result.status == '204') {
                        vm.status = 'Existing entry deleted.';
                        vm.status_class = 'alert alert-success fade in';
                    }
                }
                , function(err) {
                    vm.status = '[ERROR] ' + err.message;
                    vm.status_class = 'alert alert-danger fade in';
                }
            )
            .then(
                function() {
                    get_client_seats();
                    $timeout(function() {
                        vm.status = '';
                        vm.status_class = '';
                    }, 5000);
                }
            )
    };
}
