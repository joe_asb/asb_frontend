(function () {
    'use strict';

    angular
        .module('asb_main')
        .factory('CommunityService', Service);

    function Service($http, $q, __env, Upload) {
        var factory = {
              _get_communities: get_communities
            , _get_community: get_community
            , _update_community: update_community
            , _delete_community: delete_community
            , _add_community: add_community
            , _import_community: import_community
            , _export_community: export_community
            , _get_project_communities: get_project_communities
        };

        // var _status = ['Disabled', 'User', 'Admin'];
        // var _status_class = ['btn btn-danger', 'btn btn-info', 'btn btn-primary'];

        return factory;

        function serial_to_obj(rows, delim) {
            if (delim === undefined) {
                delim = ':';
            }

            var obj_rows = []
            for (var _cnt_rows in rows) {
                var obj_row = {};
                var row = rows[_cnt_rows];
                var fields = row.split(',');
                for (var _cnt_fields in fields) {
                    var _arr_field = fields[_cnt_fields].split(':'), key = _arr_field[0], value = _arr_field[1];
                    obj_row[key] = value;
                }
                obj_rows.push(obj_row);
            }
            return obj_rows;
        }

        // parameter is optional
        // TODO: maybe add community_import_filename from tab_client_project_communities
        function get_communities(form_data) {
            var community_ids = [];
            var params = '';
            var deferred = $q.defer();

            // extract id values from form_data and push them to project_ids
            if (form_data !== undefined) {
                form_data.map(function(item) { community_ids.push(item.tab_community_id); });
                params = '?tab_community_ids=' + community_ids;
            }

            $http.get(__env.api_url + ':' + __env.port + '/tab_communities.json' + params)
                .then(
                    function(response) {
                        deferred.resolve(response.data);
                    }
                    , function(data, status, headers, config) {
                        // deferred.resolve(response.data.response);
                        deferred.resolve(JSON.parse('{"response": {"method": "GET", "result": "error", "status": "' + status + '"}}'));
                    }
                );
            return deferred.promise;
        }

        function get_community(_id) {
            var deferred = $q.defer();
            $http.get(__env.api_url + ':' + __env.port + '/tab_communities/' + _id + '.json')
                .then(
                    function(response) {
                        deferred.resolve(response.data);
                    }
                    , function(data, status, headers, config) {
                        // deferred.resolve(response.data.response);
                        deferred.resolve(JSON.parse('{"response": {"method": "GET", "result": "error", "status": "' + status + '"}}'));
                    }
                );
            return deferred.promise;
        }

        function get_project_communities(_id) {
            var deferred = $q.defer();
            $http.get(__env.api_url + ":" + __env.port + "/tab_projects/" + _id + "/tab_communities.json")
                .then(
                    function(response) {
                        deferred.resolve(response.data);
                    }
                    , function(data, status, headers, config) {
                        // deferred.resolve(response.data.response);
                        deferred.resolve(JSON.parse('{"response": {"method": "GET", "result": "error", "status": "' + status + '"}}'));
                    }
                );
            return deferred.promise;
        }

        function add_community(form_data) {
            var deferred = $q.defer();
            var config = {
                headers : {
                    'Content-Type': 'application/json;charset=utf-8;'
                }
            };

            console.log(form_data);

            // since values from pulldown is in comma delimited list, list is converted in to obj
            // form_data.client = serial_to_obj(form_data.client);   // converting delimited strings into JSON objects

            $http.post(__env.api_url + ':' + __env.port + '/tab_communities', form_data, config)
                .then(function(form_data, status, headers, config) {
                    deferred.resolve(form_data);
                }
                , function(data, status, headers, config) {
                    // deferred.resolve(response.data.response);
                    deferred.resolve(JSON.parse('{"response": {"method": "POST", "result": "error", "status": "' + status + '"}}'));
                });
            return deferred.promise;
        }

        function update_community(form_data) {
            var deferred = $q.defer();
            var config = {
                headers : {
                    'Content-Type': 'application/json;charset=utf-8;'
                }
            };

            $http.put(__env.api_url + ':' + __env.port + '/tab_communities/' + form_data.id + '.json', form_data, config)
                .then(function(response) {
                    deferred.resolve(response);
                }
                , function(data, status, headers, config) {
                    deferred.resolve(JSON.parse('{"response": {"method": "PUT", "result": "error", "status": "' + status + '"}}'));
                })
            ;
            return deferred.promise;
        }

        function import_community(form_data) {
            var deferred = $q.defer();

            // using 3rd party module
            // https://github.com/danialfarid/ng-file-upload
            Upload.upload({
                url: __env.api_url + ':' + __env.port + '/tab_communities/' + form_data.id + '.json'
                , data: form_data
                , method: 'PUT'
            })
                .then(
                    function(response) {
                        deferred.resolve(response.data.response);
                    }
                    , function(data, status, headers, config) {
                        // deferred.resolve(response.data.response);
                        deferred.resolve(JSON.parse('{"response": {"method": "POST", "result": "error", "status": "' + status + '"}}'));
                    }
                );
            return deferred.promise;
        }

        function export_community(_id, limit, format) {
            var deferred = $q.defer();
            $http.get(__env.api_url + ':' + __env.port + '/tab_communities/' + _id + '/tab_accounts.' + format + '?limit=' + limit)
                .then(
                    function(response) {
                        deferred.resolve(response.data);
                    }
                    , function(data, status, headers, config) {
                        // deferred.resolve(response.data.response);
                        deferred.resolve(JSON.parse('{"response": {"method": "GET", "result": "error", "status": "' + status + '"}}'));
                    }
                );
            return deferred.promise;
        }

        function delete_community(id) {
            var deferred = $q.defer();

            $http.delete(__env.api_url + ':' + __env.port + '/tab_communities/' + id)
                .then(function(data, status, headers) {
                    deferred.resolve(data);
                }
                , function(data, status, header, config) {
                    deferred.resolve(JSON.parse('{"response": {"method": "DELETE", "result": "error", "status": "' + status + '"}}'));
                })
            ;
            return deferred.promise;
        }
    }
})();
