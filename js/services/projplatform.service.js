(function () {
    'use strict';

    angular
        .module('asb_main')
        .factory('ProjPlatformService', Service);

    function Service($http, $q, __env) {
        var factory = {
              _add_projplatform: add_projplatform
            , _get_projplatforms: get_projplatforms
        };

        // var _status = ['Disabled', 'User', 'Admin'];
        // var _status_class = ['btn btn-danger', 'btn btn-info', 'btn btn-primary'];

        return factory;

        function serial_to_obj(rows, delim) {
            if (delim === undefined) {
                delim = ':';
            }

            var obj_rows = []
            for (var _cnt_rows in rows) {
                var obj_row = {};
                var row = rows[_cnt_rows];
                var fields = row.split(',');
                for (var _cnt_fields in fields) {
                    var _arr_field = fields[_cnt_fields].split(':'), key = _arr_field[0], value = _arr_field[1];
                    obj_row[key] = value;
                }
                obj_rows.push(obj_row);
            }
            return obj_rows;
        }

        function get_projplatforms(form_data) {
            var params = '';
            var deferred = $q.defer();

            // extract id values from form_data and push them to params
            console.log(form_data);
            if (form_data !== undefined) {
                Object.keys(form_data).forEach(function(key) {
                    if (key == 'tab_project_ids') {
                        params += '&' + key + '=' + form_data[key].join();
                    }
                    else {
                        params += '&' + key + '=' + form_data[key];
                    }
                });
                params = '?' + params.substring(1);
            }
            console.log(params);

            $http.get(__env.api_url + ':' + __env.port + '/tab_proj_platforms' + params)
                .then(
                    function(response) {
                        console.log(response);
                        deferred.resolve(response);
                    }
                    , function(data, status, headers, config) {
                        // deferred.resolve(response.data.response);
                        deferred.resolve(JSON.parse('{"response": {"method": "GET", "result": "error", "status": "' + status + '"}}'));
                    }
                );
            return deferred.promise;
        }

        function add_projplatform(form_data) {
            var deferred = $q.defer();
            var config = {
                headers : {
                    'Content-Type': 'application/json;charset=utf-8;'
                }
            };

            console.log(form_data);

            $http.post(__env.api_url + ':' + __env.port + '/tab_proj_platforms.json', form_data, config)
                .then(function(form_data, status, headers, config) {
                    deferred.resolve(form_data);
                }
                , function(data, status, headers, config) {
                    // deferred.resolve(response.data.response);
                    deferred.resolve(JSON.parse('{"response": {"method": "POST", "result": "error", "status": "' + status + '"}}'));
                });
            return deferred.promise;
        }
    }
})();
