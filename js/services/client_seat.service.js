(function () {
    'use strict';

    angular
        .module('asb_main')
        .factory('ClientSeatsService', Service);

    function Service($http, $q, __env) {
        var factory = {
              _get_client_seats: get_client_seats
            , _get_client_seat: get_client_seat
            , _update_client_seat: update_client_seat
            , _delete_client_seat: delete_client_seat
            , _add_client_seat: add_client_seat
            , _get_client_seat_projects: get_client_seat_projects
        };

        // var _status = ['Disabled', 'User', 'Admin'];
        // var _status_class = ['btn btn-danger', 'btn btn-info', 'btn btn-primary'];

        return factory;

        function serial_to_obj(rows, delim) {
            if (delim === undefined) {
                delim = ':';
            }

            var obj_rows = []
            for (var _cnt_rows in rows) {
                var obj_row = {};
                var row = rows[_cnt_rows];
                var fields = row.split(',');
                for (var _cnt_fields in fields) {
                    var _arr_field = fields[_cnt_fields].split(':'), key = _arr_field[0], value = _arr_field[1];
                    obj_row[key] = value;
                }
                obj_rows.push(obj_row);
            }
            return obj_rows;
        }

        function get_client_seats() {
            var deferred = $q.defer();
            $http.get(__env.api_url + ':' + __env.port + '/tab_client_seats.json')
                .then(
                    function(response) {
                        deferred.resolve(response.data);
                    }
                    , function(data, status, headers, config) {
                        // deferred.resolve(response.data.response);
                        deferred.resolve(JSON.parse('{"response": {"method": "GET", "result": "error", "status": "' + status + '"}}'));
                    }
                );
            return deferred.promise;
        }

        function get_client_seat(_id) {
            var deferred = $q.defer();
            $http.get(__env.api_url + ':' + __env.port + '/tab_client_seats/' + _id + '.json')
                .then(
                    function(response) {
                        deferred.resolve(response.data);
                    }
                    , function(data, status, headers, config) {
                        // deferred.resolve(response.data.response);
                        deferred.resolve(JSON.parse('{"response": {"method": "GET", "result": "error", "status": "' + status + '"}}'));
                    }
                );
            return deferred.promise;
        }

        function get_client_seat_projects(user_id) {
            var deferred = $q.defer();
            $http.get(__env.api_url + ':' + __env.port + '/tab_client_seats/' + user_id + '/tab_projects.json')
                .then(
                    function(response) {
                        deferred.resolve(response);
                    }
                    , function(data, status, headers, config) {
                        // deferred.resolve(response.data.response);
                        deferred.resolve(JSON.parse('{"response": {"method": "GET", "result": "error", "status": "' + status + '"}}'));
                    }
                );
            return deferred.promise;
        }

        function add_client_seat(form_data) {
            var deferred = $q.defer();
            var config = {
                headers : {
                    'Content-Type': 'application/json;charset=utf-8;'
                }
            };

            // since values from pulldown is in comma delimited list, list is converted in to obj
            // form_data.client = serial_to_obj(form_data.client);   // converting delimited strings into JSON objects

            $http.post(__env.api_url + ':' + __env.port + '/tab_client_seats.json', form_data, config)
                .then(function(form_data, status, headers, config) {
                    deferred.resolve(form_data);
                }
                , function(data, status, headers, config) {
                    // deferred.resolve(response.data.response);
                    deferred.resolve(JSON.parse('{"response": {"method": "POST", "result": "error", "status": "' + status + '"}}'));
                });
            return deferred.promise;
        }

        function update_client_seat(form_data) {
            var deferred = $q.defer();
            var config = {
                headers : {
                    'Content-Type': 'application/json;charset=utf-8;'
                }
            };

            $http.put(__env.api_url + ':' + __env.port + '/tab_client_seats/' + form_data.id + '.json', form_data, config)
                .then(function(response) {
                    deferred.resolve(response);
                }
                , function(data, status, headers, config) {
                    deferred.resolve(JSON.parse('{"response": {"method": "PUT", "result": "error", "status": "' + status + '"}}'));
                })
            ;
            return deferred.promise;
        }

        function delete_client_seat(id) {
            var deferred = $q.defer();

            $http.delete(__env.api_url + ':' + __env.port + '/tab_client_seats/' + id)
                .then(function(data, status, headers) {
                    deferred.resolve(data);
                }
                , function(data, status, header, config) {
                    deferred.resolve(JSON.parse('{"response": {"method": "DELETE", "result": "error", "status": "' + status + '"}}'));
                })
            ;
            return deferred.promise;
        }
    }
})();
