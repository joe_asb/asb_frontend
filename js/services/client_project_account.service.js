(function () {
    'use strict';

    angular
        .module('asb_main')
        .factory('ClientProjectAccountService', Service);

    function Service($http, $q, __env, Upload) {
        var factory = {
              _get_client_project_account: get_client_project_account
            , _add_client_project_account: add_client_project_account
            , _update_client_project_account: update_client_project_account
            , _delete_client_project_account: delete_client_project_account
        };

        // var _status = ['Disabled', 'User', 'Admin'];
        // var _status_class = ['btn btn-danger', 'btn btn-info', 'btn btn-primary'];

        return factory;

        function serial_to_obj(rows, delim) {
            if (delim === undefined) {
                delim = ':';
            }

            var obj_rows = []
            for (var _cnt_rows in rows) {
                var obj_row = {};
                var row = rows[_cnt_rows];
                var fields = row.split(',');
                for (var _cnt_fields in fields) {
                    var _arr_field = fields[_cnt_fields].split(':'), key = _arr_field[0], value = _arr_field[1];
                    obj_row[key] = value;
                }
                obj_rows.push(obj_row);
            }
            return obj_rows;
        }

        // TODO: find out why running tab_client_project_accounts.json GET with account and project IDs won't return single item - JWT 22 Jan 2018
        function get_client_project_account(form_data) {
            var deferred = $q.defer();

            // build query string from form_data object
            var params = "";

            // If no params are passed do not call endpoint and return error msg
            if (form_data === undefined) {
                return JSON.parse('{"response": {"method": "GET", "result": "error", "status": "no parameters provided"}}');
            }

            Object.keys(form_data).forEach(function(key) {
                if (key !== "record_constraints") {
                    params += '&' + key + '=' + form_data[key];
                }
                else {
                    params += "&limit=" + form_data.record_constraints.limit;
                    params += "&offset=" + form_data.record_constraints.offset;
                }
            });
            params = '?' + params.substring(1);

            $http.get(__env.api_url + ':' + __env.port + '/tab_client_project_accounts.json' + params)
                .then(
                    function(response) {
                        deferred.resolve(response.data);
                    }
                    , function(data, status, headers, config) {
                        // deferred.resolve(response.data.response);
                        deferred.resolve(JSON.parse('{"response": {"method": "GET", "result": "error", "status": "' + status + '"}}'));
                    });
            return deferred.promise;
        }

        function add_client_project_account(form_data) {
            var deferred = $q.defer();
            var config = {
                headers : {
                    'Content-Type': 'application/json;charset=utf-8;'
                }
            };

            console.log(form_data);

            $http.post(__env.api_url + ':' + __env.port + '/tab_client_project_accounts.json', form_data, config)
                .then(
                    function(response) {
                        deferred.resolve(response.data);
                    }
                    , function(data, status, headers, config) {
                        // deferred.resolve(response.data.response);
                        deferred.resolve(JSON.parse('{"response": {"method": "GET", "result": "error", "status": "' + status + '"}}'));
                    });
            return deferred.promise;
        }

        function update_client_project_account(form_data) {
            var deferred = $q.defer();
            var config = {
                headers : {
                    'Content-Type': 'application/json;charset=utf-8;'
                }
            };

            $http.put(__env.api_url + ':' + __env.port + '/tab_client_project_accounts/' + form_data.id + '.json', form_data, config)
                .then(function(response) {
                        deferred.resolve(response);
                    }
                    , function(data, status, headers, config) {
                        deferred.resolve(JSON.parse('{"response": {"method": "PUT", "result": "error", "status": "' + status + '"}}'));
                    })
            ;
            return deferred.promise;
        }

        // TODO: check if community to be deleted still has existing relationships
        function delete_client_project_account(_id) {
            var deferred = $q.defer();

            $http.delete(__env.api_url + ':' + __env.port + '/tab_client_project_accounts/' + _id + '.json')
                .then(function(form_data, status, headers, config) {
                        deferred.resolve(form_data);
                    }
                    , function(data, status, headers, config) {
                        // deferred.resolve(response.data.response);
                        deferred.resolve(JSON.parse('{"response": {"method": "DELETE", "result": "error", "status": "' + status + '"}}'));
                    });
            return deferred.promise;
        }
    }
})();
