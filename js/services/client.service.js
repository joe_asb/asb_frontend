(function () {
    'use strict';

    angular
        .module('asb_main')
        .factory('ClientsService', Service);

    function Service($http, $q, __env) {
        var factory = {
              _get_clients: get_clients
            , _get_client: get_client
            , _get_client_by_name: get_client_by_name
            , _get_client_projects: get_client_projects
            , _get_client_project: get_client_project
            , _update_client: update_client
            , _delete_client: delete_client
            , _add_client: add_client
        };

        // var _status = ['Disabled', 'User', 'Admin'];
        // var _status_class = ['btn btn-danger', 'btn btn-info', 'btn btn-primary'];

        return factory;

        function serial_to_obj(rows, delim) {
            if (delim === undefined) {
                delim = ':';
            }

            var obj_rows = []
            for (var _cnt_rows in rows) {
                var obj_row = {};
                var row = rows[_cnt_rows];
                var fields = row.split(',');
                for (var _cnt_fields in fields) {
                    var _arr_field = fields[_cnt_fields].split(':'), key = _arr_field[0], value = _arr_field[1];
                    obj_row[key] = value;
                }
                obj_rows.push(obj_row);
            }
            return obj_rows;
        }

        function get_clients() {
            var deferred = $q.defer();
            $http.get(__env.api_url + ':' + __env.port + '/tab_clients')
                .then(
                    function(response) {
                        deferred.resolve(response);
                    }
                    , function(data, status, headers, config) {
                        // deferred.resolve(response.data.response);
                        deferred.resolve(JSON.parse('{"response": {"method": "GET", "result": "error", "status": "' + status + '"}}'));
                    }
                );
            return deferred.promise;
        }

        function get_client(_id) {
            var deferred = $q.defer();
            $http.get(__env.api_url + ':' + __env.port + '/tab_clients/' + _id + '.json')
                .then(
                    function(response) {
                        deferred.resolve(response);
                    }
                    , function(data, status, headers, config) {
                        // deferred.resolve(response.data.response);
                        deferred.resolve(JSON.parse('{"response": {"method": "GET", "result": "error", "status": "' + status + '"}}'));
                    }
                );
            return deferred.promise;
        }

        function get_client_by_name(_name) {
            var deferred = $q.defer();
            $http.get(__env.api_url + ':' + __env.port + '/tab_clients.json?name=' + _name)
                .then(
                    function(response) {
                        deferred.resolve(response);
                    }
                    , function(data, status, headers, config) {
                        // deferred.resolve(response.data.response);
                        deferred.resolve(JSON.parse('{"response": {"method": "GET", "result": "error", "status": "' + status + '"}}'));
                    }
                );
            return deferred.promise;
        }

        function get_client_projects(_id) {
            var deferred = $q.defer();
            $http.get(__env.api_url + ':' + __env.port + '/tab_clients/' + _id + '/tab_projects')
                .then(
                    function(response) {
                        deferred.resolve(response);
                    }
                    , function(data, status, headers, config) {
                        // deferred.resolve(response.data.response);
                        deferred.resolve(JSON.parse('{"response": {"method": "GET", "result": "error", "status": "' + status + '"}}'));
                    }
                );
            return deferred.promise;
        }

        function get_client_project(client_id, project_id) {
            var deferred = $q.defer();
            $http.get(__env.api_url + ':' + __env.port + '/tab_clients/' + client_id + '/tab_projects/' + project_id)
                .then(
                    function(response) {
                        deferred.resolve(response);
                    }
                    , function(data, status, headers, config) {
                        // deferred.resolve(response.data.response);
                        deferred.resolve(JSON.parse('{"response": {"method": "GET", "result": "error", "status": "' + status + '"}}'));
                    }
                );
            return deferred.promise;
        }

        function add_client(form_data) {
            var deferred = $q.defer();
            var config = {
                headers : {
                    'Content-Type': 'application/json;charset=utf-8;'
                }
            }

            console.log(form_data);

            // since values from pulldown is in comma delimited list, list is converted in to obj
            // form_data.client = serial_to_obj(form_data.client);   // converting delimited strings into JSON objects

            $http.post(__env.api_url + ':' + __env.port + '/tab_clients', form_data, config)
                .then(function(form_data, status, headers, config) {
                    deferred.resolve(form_data);
                }
                , function(data, status, headers, config) {
                    // deferred.resolve(response.data.response);
                    deferred.resolve(JSON.parse('{"response": {"method": "POST", "result": "error", "status": "' + status + '"}}'));
                });
            return deferred.promise;
        }

        function update_client(form_data) {
            var deferred = $q.defer();
            var config = {
                headers : {
                    'Content-Type': 'application/json;charset=utf-8;'
                }
            };

            $http.put(__env.api_url + ':' + __env.port + '/tab_clients/' + form_data.id, form_data, config)
                .then(function(response) {
                    deferred.resolve(response);
                }
                , function(data, status, headers, config) {
                    deferred.resolve(JSON.parse('{"response": {"method": "PUT", "result": "error", "status": "' + status + '"}}'));
                })
            ;
            return deferred.promise;
        }

        function delete_client(id) {
            var deferred = $q.defer();

            $http.delete(__env.api_url + ':' + __env.port + '/tab_clients/' + id)
                .then(function(data, status, headers) {
                    deferred.resolve(data);
                }
                , function(data, status, header, config) {
                    deferred.resolve(JSON.parse('{"response": {"method": "DELETE", "result": "error", "status": "' + status + '"}}'));
                })
            ;
            return deferred.promise;
        }
    }
})();
