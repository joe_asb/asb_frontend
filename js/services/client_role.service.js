(function () {
    'use strict';

    angular
        .module('asb_main')
        .factory('ClientRolesService', Service);

    function Service($http, $q, __env) {
        var factory = {
              _get_client_roles: get_client_roles
            , _get_client_role: get_client_role
            , _update_client_role: update_client_role
            , _delete_client_role: delete_client_role
            , _add_client_role: add_client_role
        };

        // var _status = ['Disabled', 'User', 'Admin'];
        // var _status_class = ['btn btn-danger', 'btn btn-info', 'btn btn-primary'];

        return factory;

        function serial_to_obj(rows, delim) {
            if (delim === undefined) {
                delim = ':';
            }

            var obj_rows = []
            for (var _cnt_rows in rows) {
                var obj_row = {};
                var row = rows[_cnt_rows];
                var fields = row.split(',');
                for (var _cnt_fields in fields) {
                    var _arr_field = fields[_cnt_fields].split(':'), key = _arr_field[0], value = _arr_field[1];
                    obj_row[key] = value;
                }
                obj_rows.push(obj_row);
            }
            return obj_rows;
        }

        function get_client_roles() {
            var deferred = $q.defer();
            $http.get(__env.api_url + ':' + __env.port + '/ref_client_roles')
                .then(
                    function(response) {
                        deferred.resolve(response.data);
                    }
                    , function(data, status, headers, config) {
                        // deferred.resolve(response.data.response);
                        deferred.resolve(JSON.parse('{"response": {"method": "GET", "result": "error", "status": "' + status + '"}}'));
                    }
                );
            return deferred.promise;
        }

        function get_client_role(_id) {
            var deferred = $q.defer();
            $http.get(__env.api_url + ':' + __env.port + '/ref_client_roles/' + _id + '.json')
                .then(
                    function(response) {
                        deferred.resolve(response.data);
                    }
                    , function(data, status, headers, config) {
                        // deferred.resolve(response.data.response);
                        deferred.resolve(JSON.parse('{"response": {"method": "GET", "result": "error", "status": "' + status + '"}}'));
                    }
                );
            return deferred.promise;
        }

        function add_client_role(form_data) {
            var deferred = $q.defer();
            var config = {
                headers : {
                    'Content-Type': 'application/json;charset=utf-8;'
                }
            }

            // since values from pulldown is in comma delimited list, list is converted in to obj
            form_data.repos = serial_to_obj(form_data.repos);   // converting delimited strings into JSON objects

            $http.post(__env.api_url + ':' + __env.port + '/ref_client_roles', form_data, config)
                .then(function(form_data, status, headers, config) {
                    deferred.resolve(form_data);
                }
                , function(data, status, headers, config) {
                    // deferred.resolve(response.data.response);
                    deferred.resolve(JSON.parse('{"response": {"method": "POST", "result": "error", "status": "' + status + '"}}'));
                });
            return deferred.promise;
        }

        function update_client_role(id, form_data) {
            var deferred = $q.defer();
            var config = {
                headers : {
                    'Content-Type': 'application/json;charset=utf-8;'
                }
            };

            $http.put(__env.api_url + ':' + __env.port + '/ref_client_roles/' + id, form_data, config)
                .then(function(form_data, status, headers, config) {
                    deferred.resolve(form_data);
                }
                , function(data, status, headers, config) {
                    deferred.resolve(JSON.parse('{"response": {"method": "PUT", "result": "error", "status": "' + status + '"}}'));
                })
            ;
            return deferred.promise;
        }

        function delete_client_role(id) {
            var deferred = $q.defer();

            $http.delete(__env.api_url + ':' + __env.port + '/ref_client_roles/' + id)
                .then(function(data, status, headers) {
                    deferred.resolve(data);
                }
                , function(data, status, header, config) {
                    deferred.resolve(JSON.parse('{"response": {"method": "DELETE", "result": "error", "status": "' + status + '"}}'));
                })
            ;
            return deferred.promise;
        }
    }
})();
