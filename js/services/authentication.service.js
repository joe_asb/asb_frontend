/**
 * Created by joe_t on 2/20/2017.
 */

// code based on http://jasonwatmore.com/post/2016/04/05/AngularJS-JWT-Authentication-Example-Tutorial.aspx
(function () {
    'use strict';

    angular
        .module('asb_main')
        .factory('AuthenticationService', Service);

    function Service($http, $localStorage, $sessionStorage, jwtHelper) {
        var service = {};

        service.Login = Login;
        service.Logout = Logout;

        return service;

        function Login(email, password, callback) {
            $http.post(__env.api_url + ':' + __env.port + '/authenticate', { email: email, password: password })
            // var data_as_json = angular.toJson({"username": username, "password": password});
            // var headers =
            // $http.post('/auth', data_as_json)
                .then(function success(response) {
                        // login successful if there's a token in the response
                        // code below based on http://www.redotheweb.com/2015/11/09/api-security.html
                        if (response.data.auth_token) {
                            // store username, userid, and token in local storage to keep user logged in between page refreshes
                            // for handling CSRF
                            var decoded_token = jwtHelper.decodeToken(response.data.auth_token);

                            var _currentUser = {
                                email: email
                                , user_id: decoded_token.user_id
                                , client_id: decoded_token.client_id
                                , role_id: decoded_token.role_id
                                , name: decoded_token.name
                                , token: response.data.auth_token
                            };

                            $localStorage.currentUser = _currentUser;

                            // add jwt token to auth header for all requests made by the $http service
                            $http.defaults.headers.common.Authorization = response.data.auth_token;

                            // add token to session
                            // for handling XSS
                            $sessionStorage.currentUser = _currentUser;

                            // execute callback with true to indicate successful login
                            callback(true);
                        } else {
                            // execute callback with false to indicate failed login
                            callback(false);
                        }
                    }
                    , function error(response) {
                        callback(false);
                    }
                )
                .then(function(data, status) {
                    // execute callback with false to indicate failed login
                    callback(false);
                });
        }

        function Logout() {
            // remove user from local storage and clear http auth header
            delete $localStorage.currentUser;
            delete $localStorage.user;
            // remove user from session
            delete $sessionStorage.currentUser;
            $http.defaults.headers.common.Authorization = '';
        }
    }
})();
