(function () {
    'use strict';

    angular
        .module('asb_main')
        .factory('ProjectsService', Service);

    function Service($http, $q, __env) {
        var factory = {
              _get_projects: get_projects
            , _get_project: get_project
            , _get_project_by_name: get_project_by_name
            , _update_project: update_project
            , _delete_project: delete_project
            , _add_project: add_project
        };

        // var _status = ['Disabled', 'User', 'Admin'];
        // var _status_class = ['btn btn-danger', 'btn btn-info', 'btn btn-primary'];

        return factory;

        function serial_to_obj(rows, delim) {
            if (delim === undefined) {
                delim = ':';
            }

            var obj_rows = []
            for (var _cnt_rows in rows) {
                var obj_row = {};
                var row = rows[_cnt_rows];
                var fields = row.split(',');
                for (var _cnt_fields in fields) {
                    var _arr_field = fields[_cnt_fields].split(':'), key = _arr_field[0], value = _arr_field[1];
                    obj_row[key] = value;
                }
                obj_rows.push(obj_row);
            }
            return obj_rows;
        }


        function get_projects(form_data) {
            var params = '';
            var deferred = $q.defer();

            // extract id values from form_data and push them to params
            if (form_data !== undefined) {
                Object.keys(form_data).forEach(function(key) {
                    params += '&' + key + '=' + form_data[key];
                });
                params = '?' + params.substring(1);
            }

            console.log(form_data);

            $http.get(__env.api_url + ':' + __env.port + '/tab_projects.json' + params)
                .then(
                    function(response) {
                        deferred.resolve(response.data);
                    }
                    , function(data, status, headers, config) {
                        // deferred.resolve(response.data.response);
                        deferred.resolve(JSON.parse('{"response": {"method": "GET", "result": "error", "status": "' + status + '"}}'));
                    }
                );
            return deferred.promise;
        }

        function get_project(_id) {
            var deferred = $q.defer();
            $http.get(__env.api_url + ':' + __env.port + '/tab_projects/' + _id + '.json')
                .then(
                    function(response) {
                        deferred.resolve(response.data);
                    }
                    , function(data, status, headers, config) {
                        // deferred.resolve(response.data.response);
                        deferred.resolve(JSON.parse('{"response": {"method": "GET", "result": "error", "status": "' + status + '"}}'));
                    }
                );
            return deferred.promise;
        }

        function get_project_by_name(form_data) {
            var params = "";
            var deferred = $q.defer();

            // extract id values from form_data and push them to params
            if (form_data !== undefined) {
                Object.keys(form_data).forEach(function(key) {
                    params += "&" + key + "=" + form_data[key];
                });
                params = "?" + params.substring(1);

                $http.get(__env.api_url + ":" + __env.port + "/tab_projects.json" + params)
                    .then(
                        function(response) {
                            deferred.resolve(response.data);
                        }
                        , function(data, status, headers, config) {
                            // deferred.resolve(response.data.response);
                            deferred.resolve(JSON.parse('{"response": {"method": "GET", "result": "error", "status": "' + status + '"}}'));
                        }
                    );
            }
            else {
                deferred.resolve(JSON.parse('{"response": {"method": "GET", "result": "error", "status": "No parameters provided."}}'));
            }

            return deferred.promise;
        }

        function get_projects_by_client(_id) {
            var deferred = $q.defer();
            $http.get(__env.api_url + ':' + __env.port + '/tab_projects/' + _id + '.json')
                .then(
                    function(response) {
                        deferred.resolve(response.data);
                    }
                    , function(data, status, headers, config) {
                        // deferred.resolve(response.data.response);
                        deferred.resolve(JSON.parse('{"response": {"method": "GET", "result": "error", "status": "' + status + '"}}'));
                    }
                );
            return deferred.promise;
        }

        function add_project(form_data) {
            var deferred = $q.defer();
            var config = {
                headers : {
                    'Content-Type': 'application/json;charset=utf-8;'
                }
            };

            console.log(form_data);

            // since values from pulldown is in comma delimited list, list is converted in to obj
            // form_data.client = serial_to_obj(form_data.client);   // converting delimited strings into JSON objects

            $http.post(__env.api_url + ':' + __env.port + '/tab_projects.json', form_data, config)
                .then(function(form_data, status, headers, config) {
                    deferred.resolve(form_data);
                }
                , function(data, status, headers, config) {
                    // deferred.resolve(response.data.response);
                    deferred.resolve(JSON.parse('{"response": {"method": "POST", "result": "error", "status": "' + status + '"}}'));
                });
            return deferred.promise;
        }

        function update_project(form_data) {
            var deferred = $q.defer();
            var config = {
                headers : {
                    'Content-Type': 'application/json;charset=utf-8;'
                }
            };

            $http.put(__env.api_url + ':' + __env.port + '/tab_projects/' + form_data.id + '.json', form_data, config)
                .then(function(response) {
                    deferred.resolve(response);
                }
                , function(data, status, headers, config) {
                    deferred.resolve(JSON.parse('{"response": {"method": "PUT", "result": "error", "status": "' + status + '"}}'));
                })
            ;
            return deferred.promise;
        }

        function delete_project(id) {
            var deferred = $q.defer();

            $http.delete(__env.api_url + ':' + __env.port + '/tab_projects/' + id + '.json')
                .then(function(data, status, headers) {
                    deferred.resolve(data);
                }
                , function(data, status, header, config) {
                    deferred.resolve(JSON.parse('{"response": {"method": "DELETE", "result": "error", "status": "' + status + '"}}'));
                })
            ;
            return deferred.promise;
        }
    }
})();
