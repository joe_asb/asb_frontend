(function () {
    'use strict';

    angular
        .module('asb_main')
        .factory('ClientProjectCommunityService', Service);

    function Service($http, $q, __env, Upload) {
        var factory = {
              _get_client_project_community: get_client_project_community
            , _add_client_project_community: add_client_project_community
        };

        // var _status = ['Disabled', 'User', 'Admin'];
        // var _status_class = ['btn btn-danger', 'btn btn-info', 'btn btn-primary'];

        return factory;

        function serial_to_obj(rows, delim) {
            if (delim === undefined) {
                delim = ':';
            }

            var obj_rows = []
            for (var _cnt_rows in rows) {
                var obj_row = {};
                var row = rows[_cnt_rows];
                var fields = row.split(',');
                for (var _cnt_fields in fields) {
                    var _arr_field = fields[_cnt_fields].split(':'), key = _arr_field[0], value = _arr_field[1];
                    obj_row[key] = value;
                }
                obj_rows.push(obj_row);
            }
            return obj_rows;
        }

        function get_client_project_community(form_data) {
            // var project_ids = [];
            var params = "";
            var deferred = $q.defer();

            // If no params are passed do not call endpoint and return error msg
            if (form_data === undefined) {
                return JSON.parse('{"response": {"method": "GET", "result": "error", "status": "no parameters provided"}}');
            }

            // TODO: form_data may be refactored in the future to pass in multiple IDs as array, leaving as single value entry for now - JWT 4 Jun 2018
            params = "?tab_project_ids=" + form_data.project_id;

            // // extract id values from form_data and push them to project_ids
            // if (form_data !== undefined) {
            //     form_data.map(function(item) { project_ids.push(item.id); });
            //     params = '?tab_project_ids=' + project_ids;
            // }

            $http.get(__env.api_url + ':' + __env.port + '/tab_client_project_communities.json' + params)
                .then(
                    function(response) {
                        deferred.resolve(response.data);
                    }
                    , function(data, status, headers, config) {
                        // deferred.resolve(response.data.response);
                        deferred.resolve(JSON.parse('{"response": {"method": "GET", "result": "error", "status": "' + status + '"}}'));
                    });
            return deferred.promise;
        }

        function add_client_project_community(form_data) {
            var deferred = $q.defer();

            // using 3rd party module
            // https://github.com/danialfarid/ng-file-upload
            Upload.upload({
                url: __env.api_url + ':' + __env.port + '/tab_client_project_communities.json'
                , data: form_data
                , method: 'POST'
            })
                .then(
                    function(response) {
                        deferred.resolve(response);
                    }
                    , function(data, status, headers, config) {
                        // deferred.resolve(response.data.response);
                        deferred.resolve(JSON.parse('{"response": {"method": "POST", "result": "error", "status": "' + status + '"}}'));
                    }
                );
            return deferred.promise;
        }

        function update_community(form_data) {
            console.log(form_data);
            var deferred = $q.defer();
            var config = {
                headers : {
                    'Content-Type': 'application/json;charset=utf-8;'
                }
            };

            $http.put(__env.api_url + ':' + __env.port + '/tab_communities/' + form_data.id + '.json', form_data, config)
                .then(function(response) {
                    deferred.resolve(response);
                }
                , function(data, status, headers, config) {
                    deferred.resolve(JSON.parse('{"response": {"method": "PUT", "result": "error", "status": "' + status + '"}}'));
                })
            ;
            return deferred.promise;
        }

        function import_community(form_data) {
            console.log(form_data);
            var deferred = $q.defer();

            // using 3rd party module
            // https://github.com/danialfarid/ng-file-upload
            Upload.upload({
                url: __env.api_url + ':' + __env.port + '/tab_client_project_communities/'
                , data: form_data
                , method: 'POST'
            })
                .then(
                    function(response) {
                        deferred.resolve(response);
                    }
                    , function(data, status, headers, config) {
                        // deferred.resolve(response.data.response);
                        deferred.resolve(JSON.parse('{"response": {"method": "POST", "result": "error", "status": "' + status + '"}}'));
                    }
                );
            return deferred.promise;
        }

        function delete_community(id) {
            var deferred = $q.defer();

            $http.delete(__env.api_url + ':' + __env.port + '/tab_communities/' + id)
                .then(function(data, status, headers) {
                    deferred.resolve(data);
                }
                , function(data, status, header, config) {
                    deferred.resolve(JSON.parse('{"response": {"method": "DELETE", "result": "error", "status": "' + status + '"}}'));
                })
            ;
            return deferred.promise;
        }
    }
})();
