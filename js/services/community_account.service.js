(function () {
    'use strict';

    angular
        .module('asb_main')
        .factory('CommunityAccountService', Service);

    function Service($http, $q, __env, Upload) {
        var factory = {
              _get_community_accounts: get_community_accounts
            , _update_community_account: update_community_account
            , _delete_community_account: delete_community_account
            , _add_community_account: add_community_account
        };

        // var _status = ['Disabled', 'User', 'Admin'];
        // var _status_class = ['btn btn-danger', 'btn btn-info', 'btn btn-primary'];

        return factory;

        function serial_to_obj(rows, delim) {
            if (delim === undefined) {
                delim = ':';
            }

            var obj_rows = []
            for (var _cnt_rows in rows) {
                var obj_row = {};
                var row = rows[_cnt_rows];
                var fields = row.split(',');
                for (var _cnt_fields in fields) {
                    var _arr_field = fields[_cnt_fields].split(':'), key = _arr_field[0], value = _arr_field[1];
                    obj_row[key] = value;
                }
                obj_rows.push(obj_row);
            }
            return obj_rows;
        }

        // parameter is optional
        // TODO: maybe add community_import_filename from tab_client_project_communities
        function get_community_accounts(form_data) {
            console.log('[43] Got here');
            var deferred = $q.defer();

            // build query string from form_data object
            var params = "";

            // If no params are passed do not call endpoint and return error msg
            if (form_data === undefined) {
                return JSON.parse('{"response": {"method": "GET", "result": "error", "status": "no parameters provided"}}');
            }

            Object.keys(form_data).forEach(function(key) {
                if (key === "function") {
                    params += "&function=" + form_data["function"];
                }
                else {
                    params += "&" + key + "=" + form_data[key];
                }
            });
            params = "?" + params.substring(1);

            $http.get(__env.api_url + ':' + __env.port + '/tab_community_accounts.json' + params)
                .then(
                    function(response) {
                        deferred.resolve(response.data);
                    }
                    , function(data, status, headers, config) {
                        // deferred.resolve(response.data.response);
                        deferred.resolve(JSON.parse('{"response": {"method": "GET", "result": "error", "status": "' + status + '"}}'));
                    }
                );
            return deferred.promise;
        }

        function add_community_account(form_data) {
            var deferred = $q.defer();
            var config = {
                headers : {
                    'Content-Type': 'application/json;charset=utf-8;'
                }
            };

            console.log(form_data);

            // since values from pulldown is in comma delimited list, list is converted in to obj
            // form_data.client = serial_to_obj(form_data.client);   // converting delimited strings into JSON objects

            $http.post(__env.api_url + ':' + __env.port + '/tab_communities', form_data, config)
                .then(function(form_data, status, headers, config) {
                    deferred.resolve(form_data);
                }
                , function(data, status, headers, config) {
                    // deferred.resolve(response.data.response);
                    deferred.resolve(JSON.parse('{"response": {"method": "POST", "result": "error", "status": "' + status + '"}}'));
                });
            return deferred.promise;
        }

        function update_community_account(form_data) {
            var deferred = $q.defer();
            var config = {
                headers : {
                    'Content-Type': 'application/json;charset=utf-8;'
                }
            };

            $http.put(__env.api_url + ':' + __env.port + '/tab_community_accounts/' + form_data.id + '.json', form_data, config)
                .then(function(response) {
                    deferred.resolve(response);
                }
                , function(data, status, headers, config) {
                    deferred.resolve(JSON.parse('{"response": {"method": "PUT", "result": "error", "status": "' + status + '"}}'));
                })
            ;
            return deferred.promise;
        }

        function delete_community_account(id) {
            var deferred = $q.defer();

            $http.delete(__env.api_url + ':' + __env.port + '/tab_community_accounts/' + id)
                .then(function(data, status, headers) {
                    deferred.resolve(data);
                }
                , function(data, status, header, config) {
                    deferred.resolve(JSON.parse('{"response": {"method": "DELETE", "result": "error", "status": "' + status + '"}}'));
                })
            ;
            return deferred.promise;
        }
    }
})();
