(function () {
    'use strict';

    angular
        .module('asb_main')
        .factory('AccountService', Service);

    function Service($http, $q, __env) {
        var factory = {
              _get_accounts: get_accounts
            , _get_account: get_account
            , _get_accounts_by_project: get_accounts_by_project
            , _update_account: update_account
            , _delete_account: delete_account
            , _add_account: add_account
        };

        // var _status = ['Disabled', 'User', 'Admin'];
        // var _status_class = ['btn btn-danger', 'btn btn-info', 'btn btn-primary'];

        return factory;

        function serial_to_obj(rows, delim) {
            if (delim === undefined) {
                delim = ':';
            }

            var obj_rows = []
            for (var _cnt_rows in rows) {
                var obj_row = {};
                var row = rows[_cnt_rows];
                var fields = row.split(',');
                for (var _cnt_fields in fields) {
                    var _arr_field = fields[_cnt_fields].split(':'), key = _arr_field[0], value = _arr_field[1];
                    obj_row[key] = value;
                }
                obj_rows.push(obj_row);
            }
            return obj_rows;
        }

        function get_accounts(form_data) {
            var deferred = $q.defer();
            if (! form_data) {
                return JSON.parse('{"response": {"method": "GET", "result": "error", "message": "No params provided."}}')
            }

            var params = '';
            params += 'offset=' + form_data.record_constraints.offset;
            params += '&limit=' + form_data.record_constraints.limit;

            $http.get(__env.api_url + ':' + __env.port + '/tab_accounts.json?' + params)
                .then(
                    function(response) {
                        deferred.resolve(response.data);
                    }
                    , function(data, status, headers, config) {
                        // deferred.resolve(response.data.response);
                        deferred.resolve(JSON.parse('{"response": {"method": "GET", "result": "error", "status": "' + status + '"}}'));
                    }
                );
            return deferred.promise;
        }

        function get_account(_id) {
            var deferred = $q.defer();
            $http.get(__env.api_url + ':' + __env.port + '/tab_accounts/' + _id + '.json')
                .then(
                    function(response) {
                        deferred.resolve(response.data);
                    }
                    , function(data, status, headers, config) {
                        // deferred.resolve(response.data.response);
                        deferred.resolve(JSON.parse('{"response": {"method": "GET", "result": "error", "status": "' + status + '"}}'));
                    }
                );
            return deferred.promise;
        }

        function get_accounts_by_project(_id, _list_data) {
            var deferred = $q.defer();
            var inc_list_data = (parseInt(_list_data) === 1 ? "?list_data=1" : "");

            $http.get(__env.api_url + ":" + __env.port + "/tab_projects/" + _id + "/tab_accounts.json" + inc_list_data)
                .then(
                    function(response) {
                        deferred.resolve(response.data);
                    }
                    , function(data, status, headers, config) {
                        // deferred.resolve(response.data.response);
                        deferred.resolve(JSON.parse('{"response": {"method": "GET", "result": "error", "status": "' + status + '"}}'));
                    }
                );
            return deferred.promise;
        }

        // function get_accounts_by_client(_id) {
        //     var deferred = $q.defer();
        //     $http.get(__env.api_url + ':' + __env.port + '/tab_accounts/' + _id + '.json')
        //         .then(
        //             function(response) {
        //                 deferred.resolve(response.data);
        //             }
        //             , function(data, status, headers, config) {
        //                 // deferred.resolve(response.data.response);
        //                 deferred.resolve(JSON.parse('{"response": {"method": "GET", "result": "error", "status": "' + status + '"}}'));
        //             }
        //         );
        //     return deferred.promise;
        // }

        function add_account(form_data) {
            var deferred = $q.defer();
            var config = {
                headers : {
                    'Content-Type': 'application/json;charset=utf-8;'
                }
            };

            console.log(form_data);

            // since values from pulldown is in comma delimited list, list is converted in to obj
            // form_data.client = serial_to_obj(form_data.client);   // converting delimited strings into JSON objects

            $http.post(__env.api_url + ':' + __env.port + '/tab_accounts.json', form_data, config)
                .then(function(form_data, status, headers, config) {
                    deferred.resolve(form_data.data);
                }
                , function(data, status, headers, config) {
                    // deferred.resolve(response.data.response);
                    deferred.resolve(JSON.parse('{"response": {"method": "POST", "result": "error", "status": "' + status + '"}}'));
                });
            return deferred.promise;
        }

        function update_account(form_data) {
            var deferred = $q.defer();
            var config = {
                headers : {
                    'Content-Type': 'application/json;charset=utf-8;'
                }
            };

            $http.put(__env.api_url + ':' + __env.port + '/tab_accounts/' + form_data.id + '.json', form_data, config)
                .then(function(response) {
                    deferred.resolve(response);
                }
                , function(data, status, headers, config) {
                    deferred.resolve(JSON.parse('{"response": {"method": "PUT", "result": "error", "status": "' + status + '"}}'));
                })
            ;
            return deferred.promise;
        }

        function delete_account(id) {
            var deferred = $q.defer();

            $http.delete(__env.api_url + ':' + __env.port + '/tab_accounts/' + id + '.json')
                .then(function(data, status, headers) {
                    deferred.resolve(data);
                }
                , function(data, status, header, config) {
                    deferred.resolve(JSON.parse('{"response": {"method": "DELETE", "result": "error", "status": "' + status + '"}}'));
                })
            ;
            return deferred.promise;
        }
    }
})();
